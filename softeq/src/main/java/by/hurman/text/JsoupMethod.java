package by.hurman.text;

import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.IOException;

public class JsoupMethod {
    public static void main(String args[]) throws IOException {
        String page = "https://mkyong.com/java/java-how-to-get-all-links-from-a-web-page/";
        //Connecting to the web page
        Connection conn = Jsoup.connect(page);
        //executing the get request
        Document doc = conn.get();
        //Retrieving the contents (body) of the web page
        String result = doc.body().text();
        System.out.println(result);
    }
}