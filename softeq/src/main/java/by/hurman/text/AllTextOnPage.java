package by.hurman.text;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

public class AllTextOnPage {

    private static final String str = "https://mkyong.com/java/java-how-to-get-all-links-from-a-web-page/";

    public static void main(String[] args) throws IOException {

        for (String link : findLinks(str)) {
            System.out.println(link);
        }
    }

    private static Set<String> findLinks(String url) throws IOException {

        Set<String> links = new HashSet<>();

        Document doc = Jsoup.connect(url)
                .data("query", "Java")
                .userAgent("Mozilla")
                .cookie("auth", "token")
//                .timeout(3000)
                .get();

        String text = doc.body().text(); // "An example link"
        System.out.println(text);
        return links;

    }
}
