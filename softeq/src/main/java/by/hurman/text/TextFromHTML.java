package by.hurman.text;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;

public class TextFromHTML {
    public static void main(String[] args) {
        try {
            // Create a URL for the desired page
            URL url = new URL("https://mkyong.com/java/java-how-to-get-all-links-from-a-web-page/");

            // Read all the text returned by the server
            BufferedReader in = new BufferedReader(new InputStreamReader(url.openStream()));
            String str;
            while (in.readLine() != null) {
                str = in.readLine();
                System.out.println(str);
                // str is one line of text; readLine() strips the newline character(s)
            }
            in.close();
            String html = "<p>An <a href='http://example.com/'><b>example</b></a> link.</p>";
            Document doc = Jsoup.parse(html);
            String text = doc.body().text();
            System.out.println(text);
        } catch (MalformedURLException e) {
        } catch (IOException e) {
        }
    }
}
