package by.hurman_dzmitry.log;

import by.hurman_dzmitry.general.Graduation;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Log {
    private List<String> logMessages;

    public Log(List<Path> listLogAddresses) {
        logMessages = new ArrayList<>();
        singleMessageDivision(readAllFiles(listLogAddresses));
    }

    public List<String> getLogMessages() {
        return logMessages;
    }

    private String readAllFiles(List<Path> logAddresses) {
        StringBuilder log = new StringBuilder();
        for (Path path : logAddresses) {
            try {
                Files.lines(path, StandardCharsets.UTF_8).forEach((p) -> log.append(p).append("\n"));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return log.toString();
    }

    private void singleMessageDivision(String log) {
        int beginStr = 0;
        int endStr = 0;
        int numberFormatDate = Graduation.FORMAT_SECOND.getFormat().length();

        while (endStr < log.length()) {
            Pattern p = Pattern.compile("\\d{4}-\\d{2}-\\d{2}\\s\\d{2}:\\d{2}:\\d{2}\\s");
            Matcher m = p.matcher(log.substring(beginStr + numberFormatDate));
            if (m.find()) {
                endStr = m.start() + beginStr + numberFormatDate;
                logMessages.add(log.substring(beginStr, endStr));
                beginStr = endStr;
            } else {
                logMessages.add(log.substring(beginStr));
                endStr = log.length();
            }
        }
    }
}
