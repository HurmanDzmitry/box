package by.hurman_dzmitry.runner;

import by.hurman_dzmitry.user_input.Menu;

public class App {

    public static void main(String[] args) {
        new Menu().execute();
    }
}
