package by.hurman_dzmitry.runner;

import by.hurman_dzmitry.address.Addresses;
import by.hurman_dzmitry.log.Log;
import by.hurman_dzmitry.output.Grouping;
import by.hurman_dzmitry.output.Writer;

import java.io.IOException;
import java.nio.file.Path;
import java.text.ParseException;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class Builder {

    private Builder() {
    }

    public static Map<Date, Integer> createChain(String directory, Date startInterval,
                                                 Date finishInterval, String graduation,
                                                 String placeFile) throws IOException, ParseException {
        List<Path> address = new Addresses(directory).getLogAddresses();
        List<String> logMessages = new Log(address).getLogMessages();
        new Writer().outInFile(logMessages, startInterval, finishInterval, placeFile);
        return new TreeMap<>(new Grouping().getGroupingByTime(logMessages, startInterval, finishInterval, graduation));
    }

    public static Map<String, Integer> createChain(String directory, Date startInterval,
                                                   Date finishInterval, String placeFile) throws IOException, ParseException {
        List<Path> address = new Addresses(directory).getLogAddresses();
        List<String> logMessages = new Log(address).getLogMessages();
        new Writer().outInFile(logMessages, startInterval, finishInterval, placeFile);
        return new TreeMap<>(new Grouping().getGroupingByException(logMessages, startInterval, finishInterval));
    }
}
