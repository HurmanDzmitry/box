package by.hurman_dzmitry.runner;

import by.hurman_dzmitry.address.Addresses;
import by.hurman_dzmitry.general.General;
import by.hurman_dzmitry.general.Graduation;
import by.hurman_dzmitry.log.Log;
import by.hurman_dzmitry.output.Grouping;
import by.hurman_dzmitry.output.Writer;

import java.io.IOException;
import java.nio.file.Path;
import java.text.ParseException;
import java.util.*;

public class Test {

    private static String directory = System.getProperty("user.dir");
    private static Date startInterval;
    private static Date finishInterval;

    static {
        try {
            startInterval = General.dateFormat("2019-12-11 00:00:00");
            finishInterval = General.dateFormat("2019-12-14 00:00:00");
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    private static String graduation = Graduation.grad("минута");
    private static String placeFile = System.getProperty("user.dir") + "\\out.txt";


    public static void main(String[] args) throws ParseException, IOException {

        List<Path> address = new Addresses(directory).getLogAddresses();

        List<String> logMessages = new Log(address).getLogMessages();

        new Writer().outInFile(logMessages, startInterval, finishInterval, placeFile);

        Map<Date, Integer> groupingLogMessages1 = new TreeMap<>
                (new Grouping().getGroupingByTime(logMessages, startInterval, finishInterval, graduation));

        outputDate(groupingLogMessages1);

        Map<String, Integer> groupingLogMessages2 = new TreeMap<>
                (new Grouping().getGroupingByException(logMessages, startInterval, finishInterval));

        outputString(groupingLogMessages2);
    }

    private static void outputDate(Map<Date, Integer> groupingLogMessages) {
        Set<Date> dateSet = groupingLogMessages.keySet();
        for (Date date : dateSet) {
            System.out.print(General.dateToString(date, graduation) + " - ");
            System.out.println(groupingLogMessages.get(date));
        }
    }

    private static void outputString(Map<String, Integer> groupingLogMessages) {
        Set<String> stringSet = groupingLogMessages.keySet();
        for (String s : stringSet) {
            System.out.print(s + " - ");
            System.out.println(groupingLogMessages.get(s));
        }
    }
}
