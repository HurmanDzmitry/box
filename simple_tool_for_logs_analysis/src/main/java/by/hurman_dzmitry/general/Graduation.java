package by.hurman_dzmitry.general;

public enum Graduation {
    FORMAT_SECOND("yyyy-MM-dd HH:mm:ss"),
    FORMAT_MINUTE("yyyy-MM-dd HH:mm"),
    FORMAT_HOUR("yyyy-MM-dd HH"),
    FORMAT_DAY("yyyy-MM-dd"),
    FORMAT_MONTH("yyyy-MM");

    private String format;

    Graduation(String format) {
        this.format = format;
    }

    public String getFormat() {
        return format;
    }

    public static String grad(String strInput) {
        String grad;
        switch (strInput) {
            case "секунда":
                grad = FORMAT_SECOND.getFormat();
                break;
            case "минута":
                grad = FORMAT_MINUTE.getFormat();
                break;
            case "час":
                grad = FORMAT_HOUR.getFormat();
                break;
            case "день":
                grad = FORMAT_DAY.getFormat();
                break;
            case "месяц":
                grad = FORMAT_MONTH.getFormat();
                break;
            default:
                throw new IllegalArgumentException();
        }
        return grad;
    }
}