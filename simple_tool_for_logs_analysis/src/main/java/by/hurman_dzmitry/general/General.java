package by.hurman_dzmitry.general;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class General {
    private static SimpleDateFormat format = new SimpleDateFormat();

    private General() {
    }

    public static Date dateFormat(String log) throws ParseException {
        format.applyPattern(Graduation.FORMAT_SECOND.getFormat());
        return format.parse(log);
    }

    public static Date dateFormat(String log, String graduation) throws ParseException {
        format.applyPattern(graduation);
        return format.parse(log);
    }

    public static String dateToString(Date date, String graduation) {
        format.applyPattern(graduation);
        return format.format(date);
    }

    public static boolean isDuplicateKey(Map<Date, List<String>> logMessages, Date date) {
        boolean bool = false;
        for (Date d : logMessages.keySet()) {
            if (date.equals(d))
                bool = true;
        }
        return bool;
    }

    public static boolean isDuplicateKey(Set<Date> dateSet, Date date) {
        boolean bool = false;
        for (Date d : dateSet) {
            if (date.equals(d))
                bool = true;
        }
        return bool;
    }

    public static boolean isDuplicateKey(Set<String> dateSet, String str) {
        boolean bool = false;
        for (String s : dateSet) {
            if (str.equals(s))
                bool = true;
        }
        return bool;
    }
}
