package by.hurman_dzmitry.address;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class Addresses {
    private List<Path> logAddresses;

    public Addresses(String directory) throws IOException {
        logAddresses = new ArrayList<>();
        findLogAddresses(directory);
    }

    public List<Path> getLogAddresses() {
        return logAddresses;
    }

    private void findLogAddresses(String directory) throws IOException {
        logAddresses = Files.walk(Paths.get(directory))
                .filter(Files::isRegularFile).filter(this::isLog)
                .collect(Collectors.toList());
    }

    private boolean isLog(Path path) {
        Pattern p = Pattern.compile("\\w+" + "_log.txt." + "\\d+");
        Matcher m = p.matcher(path.toString());
        return m.find();
    }
}