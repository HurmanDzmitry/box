package by.hurman_dzmitry.user_input;

import by.hurman_dzmitry.general.General;
import by.hurman_dzmitry.general.Graduation;
import by.hurman_dzmitry.runner.Builder;

import java.io.IOException;
import java.text.ParseException;
import java.util.Date;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;

public class Menu {
    private static final Scanner SCANNER = new Scanner(System.in);

    private String directory;
    private Date startInterval;
    private Date finishInterval;
    private String graduation;
    private String placeFile;

    public Menu() {
    }

    public void execute() {
        setValue();
        try {
            if (graduation != null) {
                Map<Date, Integer> groupingLogMessages = Builder.createChain(directory, startInterval,
                        finishInterval, graduation, placeFile);
                outputDate(groupingLogMessages);
            } else {
                Map<String, Integer> groupingLogMessages = Builder.createChain(directory, startInterval,
                        finishInterval, placeFile);
                outputString(groupingLogMessages);
            }

        } catch (IOException e) {
            System.out.println("Ошибка ввода/вывода");
            e.printStackTrace();
            execute();
        } catch (ParseException e) {
            System.out.println("Ошибка, поробуй еще");
            execute();
        }
    }

    private void setDirectory() {
        System.out.println("Введите место поиска (формат D:\\\\Java\\\\...), по умолчанию место нахождения программы (жми ввод)");
        directory = SCANNER.nextLine();
        if (directory == null) {
            directory = System.getProperty("user.dir");
        }
    }

    private void setInterval() throws ParseException {
        System.out.println("Введите интервал времени (формат с гггг-мм-дд чч:мм:сс <ввод> по гггг-мм-дд чч:мм:сс," +
                " пример 2019-01-01 11:11:11), по умолчанию за все время (жми ввод)");
        String line = SCANNER.nextLine();
        if (line.isEmpty()) {
            startInterval = General.dateFormat("1990-01-01 00:00:00");
            finishInterval = new Date(System.currentTimeMillis());
        } else {
            startInterval = General.dateFormat(line);
            line = SCANNER.nextLine();
            finishInterval = General.dateFormat(line);
        }
    }

    private void setGraduationByTime() {
        System.out.println("Введите градацию времени, вводить: секунда, минута, час, день, месяц. По умолчанию будет день(жми ввод)");
        String line = SCANNER.nextLine();
        if (line.isEmpty()) {
            graduation = Graduation.grad("день");
        } else {
            graduation = Graduation.grad(line);
        }
    }

    private void setPlaceFile() {
        System.out.println("Введите место записи в файл (формат D:\\\\Java\\\\...), по умолчанию место нахождения программы (жми ввод)");
        placeFile = SCANNER.nextLine() + "\\out.txt";
        if (placeFile.equals("\\out.txt"))
            placeFile = System.getProperty("user.dir") + "\\out.txt";
    }

    private boolean setTypeGrouping() {
        boolean b = false;
        System.out.println("Введите тип группировки (по времени или типу ошибки): вводить \"время\" или \"ошибка\", по умолчанию время(жми ввод)");
        String line = SCANNER.nextLine();
        if (line.equals("ошибка")) {
            b = true;
        }
        return b;
    }

    private void setValue() {
        try {
            setDirectory();
            setInterval();
            if (!setTypeGrouping()) {
                setGraduationByTime();
            }
            setPlaceFile();
        } catch (Exception e) {
            System.out.println("Ошибка ввода, поробуй еще");
        }
    }

    private void outputDate(Map<Date, Integer> groupingLogMessages) {
        Set<Date> dateSet = groupingLogMessages.keySet();
        for (Date date : dateSet) {
            System.out.println(General.dateToString(date, graduation) + " - " + groupingLogMessages.get(date));
        }
    }

    private void outputString(Map<String, Integer> groupingLogMessages) {
        Set<String> stringSet = groupingLogMessages.keySet();
        for (String s : stringSet) {
            System.out.println(s + " - " + groupingLogMessages.get(s));
        }
    }
}