package by.hurman_dzmitry.output;

import by.hurman_dzmitry.general.General;
import by.hurman_dzmitry.general.Graduation;

import java.io.FileWriter;
import java.io.IOException;
import java.text.ParseException;
import java.util.*;

public class Writer {

    public void outInFile(List<String> listLog, Date startInterval, Date finishInterval, String placeFile) throws ParseException {
        Map<Date, List<String>> logMessages = new TreeMap<>(Comparator.naturalOrder());
        for (String s : listLog) {
            List<String> listStr = new ArrayList<>();
            Date date = General.dateFormat(s.substring(0, Graduation.FORMAT_SECOND.getFormat().length()));
            if (General.isDuplicateKey(logMessages, date))
                listStr = logMessages.get(date);
            if (date.after(startInterval) && date.before(finishInterval)) {
                listStr.add(s);
                logMessages.put(date, listStr);
            }
        }
        fileWriter(logMessages, placeFile);
    }

    private void fileWriter(Map<Date, List<String>> logMessages, String placeFile) {
        Thread thread = new Thread(() -> {
            Set<Date> dates = logMessages.keySet();
            try (FileWriter writer = new FileWriter(placeFile)) {
                for (Date date : dates) {
                    writer.write(String.valueOf(logMessages.get(date)));
                    writer.flush();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
        thread.start();
    }
}