package by.hurman_dzmitry.output;

import by.hurman_dzmitry.general.General;
import by.hurman_dzmitry.general.Graduation;

import java.text.ParseException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Grouping {
    public Grouping() {
    }

    public Map<Date, Integer> getGroupingByTime(List<String> listLog, Date startInterval, Date finishInterval, String graduation) throws ParseException {
        Map<Date, Integer> logMessages = new HashMap<>();
        for (String s : listLog) {
            Integer i = 1;
            Date date = General.dateFormat(s.substring(0, Graduation.FORMAT_SECOND.getFormat().length()));

            if (date.after(startInterval) && date.before(finishInterval)) {
                date = General.dateFormat(s.substring(0, graduation.length()), graduation);
                if (General.isDuplicateKey(logMessages.keySet(), date)) {
                    i = logMessages.get(date);
                    ++i;
                }
                logMessages.put(date, i);
            }
        }
        return logMessages;
    }

    public Map<String, Integer> getGroupingByException(List<String> listLog, Date startInterval, Date finishInterval) throws ParseException {
        Map<String, Integer> logMessages = new HashMap<>();
        for (String s : listLog) {
            Integer i = 1;
            String typeException = searchException(s);
            Date date = General.dateFormat(s.substring(0, Graduation.FORMAT_SECOND.getFormat().length()));

            if (date.after(startInterval) && date.before(finishInterval)) {
                if (General.isDuplicateKey(logMessages.keySet(), typeException)) {
                    i = logMessages.get(typeException);
                    ++i;
                }
                logMessages.put(typeException, i);
            }
        }
        return logMessages;
    }

    private String searchException(String logMessage) {
        String exception = "";
        Pattern p = Pattern.compile(" [A-Z_]+ ");
        Matcher m = p.matcher(logMessage);
        if (m.find()) {
            exception = logMessage.substring(m.start(), m.end());
        }
        return exception.trim();
    }
}