package by.dzmitry_hurman;

import by.dzmitry_hurman.service.SQLQueryBuilder;
import org.junit.After;
import org.junit.Assert;
import org.junit.Test;

public class MainTest {

    private SQLQueryBuilder sqb = new SQLQueryBuilder();

    @After
    public void clear() {
        sqb.clear();
    }

    @Test
    public void getQueryForFirstHalfOfTaskShouldBeValid() {
        String query = sqb.getAll().fromOne("persons")
                .where().equal("surname", "Морская").and()
                .equal("name", "Мария").and()
                .equal("patronymic", "Васильевна")
                .build();

        String expectedQuery = "select * from persons " +
                "where surname = 'Морская' and name = 'Мария' and patronymic = 'Васильевна'";

        Assert.assertEquals(expectedQuery, query);
    }

    @Test
    public void getQueryForSecondHalfOfTaskShouldBeValid() {
        String query = sqb.getAll().fromOne("persons")
                .where().equalsEndPart("surname", "ов").or()
                .equal("sex", "женщина")
                .build();

        String expectedQuery = "select * from persons " +
                "where surname like '%ов' or sex = 'женщина'";

        Assert.assertEquals(expectedQuery, query);
    }
}
