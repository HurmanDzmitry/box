package by.dzmitry_hurman;

import by.dzmitry_hurman.entity.Person;
import by.dzmitry_hurman.service.SQLQueryBuilder;
import org.hibernate.Session;

import javax.persistence.TypedQuery;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        Session session = HibernateUtil.getSessionFactory().openSession();
        SQLQueryBuilder sqb = new SQLQueryBuilder();

        String query1 = sqb.getAll().fromOne("persons")
                .where().equal("surname", "Морская").and()
                .equal("name", "Мария").and()
                .equal("patronymic", "Васильевна")
                .build();

        TypedQuery<Person> typedQuery1 = session.createSQLQuery(query1)
                .addEntity(Person.class);
        Person person = typedQuery1.getSingleResult();
        System.out.println(person);

        sqb.clear();
        System.out.println("---------");

        String query2 = sqb.getAll().fromOne("persons")
                .where().equalsEndPart("surname", "ов").or()
                .equal("sex", "женщина")
                .build();
        TypedQuery<Person> typedQuery2 = session.createSQLQuery(query2)
                .addEntity(Person.class);

        List<Person> persons = typedQuery2.getResultList();
        for (Person p : persons) {
            System.out.println(p);
        }

        session.close();
    }
}