package by.dzmitry_hurman.service;

public class SQLQueryBuilder implements QueryBuilder<String> {

    private StringBuilder query;

    public SQLQueryBuilder() {
        query = new StringBuilder();
    }

    @Override
    public SQLQueryBuilder getOne(String column) {
        query.append("select ").append(column).append(" ");
        return this;
    }

    @Override
    public SQLQueryBuilder getSome(String... columns) {
        query.append("select ");
        String joined = String.join(", ", columns);
        query.append(joined).append(" ");
        return this;
    }

    @Override
    public SQLQueryBuilder getAll() {
        query.append("select * ");
        return this;
    }

    @Override
    public SQLQueryBuilder fromOne(String table) {
        query.append("from ").append(table).append(" ");
        return this;
    }

    @Override
    public SQLQueryBuilder fromSome(String... tables) {
        query.append("from ");
        String joined = String.join(", ", tables);
        query.append(joined).append(" ");
        return this;
    }

    @Override
    public SQLQueryBuilder where() {
        query.append("where ");
        return this;
    }

    @Override
    public SQLQueryBuilder and() {
        query.append("and ");
        return this;
    }

    @Override
    public SQLQueryBuilder or() {
        query.append("or ");
        return this;
    }

    @Override
    public SQLQueryBuilder equal(String columnName, String comparingString) {
        query.append(columnName).append(" = ").append("\'").append(comparingString).append("\' ");
        return this;
    }

    @Override
    public SQLQueryBuilder equalsPart(String columnName, String comparingString) {
        query.append(columnName).append(" like \'%").append(comparingString).append("%\' ");
        return this;
    }

    @Override
    public SQLQueryBuilder equalsEndPart(String columnName, String comparingString) {
        query.append(columnName).append(" like \'%").append(comparingString).append("\' ");
        return this;
    }

    @Override
    public SQLQueryBuilder equalsBeginPart(String columnName, String comparingString) {
        query.append(columnName).append(" like \'").append(comparingString).append("%\' ");
        return this;
    }

    @Override
    public String build() {
        return query.toString().trim();
    }

    public void clear() {
        query.setLength(0);
    }
}
