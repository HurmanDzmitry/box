package by.dzmitry_hurman.service;

public interface QueryBuilder<T> {

    QueryBuilder getOne(String column);

    QueryBuilder getSome(String... columns);

    QueryBuilder getAll();

    QueryBuilder fromOne(String table);

    QueryBuilder fromSome(String... tables);

    QueryBuilder where();

    QueryBuilder and();

    QueryBuilder or();

    QueryBuilder equal(String columnName, String comparingString);

    QueryBuilder equalsPart(String columnName, String comparingString);

    QueryBuilder equalsEndPart(String columnName, String comparingString);

    QueryBuilder equalsBeginPart(String columnName, String comparingString);

    T build();
}
