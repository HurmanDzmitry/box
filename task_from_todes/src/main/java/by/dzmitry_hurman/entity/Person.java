package by.dzmitry_hurman.entity;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "persons")
public class Person {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "name", nullable = false, length = 20)
    private String name;

    @Column(name = "surname", nullable = false, length = 20)
    private String surname;

    @Column(name = "patronymic", nullable = false, length = 20)
    private String patronymic;

    @Column(name = "date_of_birth", nullable = false)
    private LocalDate dateOfBirth;

    @Column(name = "sex", nullable = false)
    private String sex;

    @Column(name = "email", length = 20)
    private String email;

    @Column(name = "telephone", nullable = false, length = 20)
    private String telephone;

    @Column(name = "repository", length = 100)
    private String repository;

    @Column(name = "skype", length = 20)
    private String skype;

    @Column(name = "linkedin", length = 100)
    private String linkedin;

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(
            name = "persons_has_technology",
            joinColumns = {@JoinColumn(name = "id_person")},
            inverseJoinColumns = {@JoinColumn(name = "id_technology")}
    )
    private Set<Technology> technologySet = new HashSet<>();

    public Person() {
    }

    public Person(String name, String surname, String patronymic, LocalDate dayOfBirth, String sex,
                  String email, String telephone, String repository, String skype, String linkedin) {
        this.name = name;
        this.surname = surname;
        this.patronymic = patronymic;
        this.dateOfBirth = dayOfBirth;
        this.sex = sex;
        this.email = email;
        this.telephone = telephone;
        this.repository = repository;
        this.skype = skype;
        this.linkedin = linkedin;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public void setPatronymic(String patronymic) {
        this.patronymic = patronymic;
    }

    public LocalDate getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(LocalDate dayOfBirth) {
        this.dateOfBirth = dayOfBirth;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getRepository() {
        return repository;
    }

    public void setRepository(String repository) {
        this.repository = repository;
    }

    public String getSkype() {
        return skype;
    }

    public void setSkype(String skype) {
        this.skype = skype;
    }

    public String getLinkedin() {
        return linkedin;
    }

    public void setLinkedin(String linkedin) {
        this.linkedin = linkedin;
    }

    public Set<Technology> getTechnologySet() {
        return technologySet;
    }

    public void setTechnologySet(Set<Technology> technologySet) {
        this.technologySet = technologySet;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Person person = (Person) o;
        return Objects.equals(name, person.name) &&
                Objects.equals(surname, person.surname) &&
                Objects.equals(patronymic, person.patronymic) &&
                Objects.equals(dateOfBirth, person.dateOfBirth) &&
                Objects.equals(sex, person.sex) &&
                Objects.equals(email, person.email) &&
                Objects.equals(telephone, person.telephone) &&
                Objects.equals(repository, person.repository) &&
                Objects.equals(skype, person.skype) &&
                Objects.equals(linkedin, person.linkedin) &&
                Objects.equals(technologySet, person.technologySet);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, surname, patronymic, dateOfBirth, sex, email, telephone, repository, skype, linkedin, technologySet);
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + "{" +
                ", name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", patronymic='" + patronymic + '\'' +
                ", dayOfBirth=" + dateOfBirth +
                ", sex=" + sex +
                ", email='" + email + '\'' +
                ", telephone='" + telephone + '\'' +
                ", repository='" + repository + '\'' +
                ", skype='" + skype + '\'' +
                ", linkedin='" + linkedin + '\'' +
                ", technologySet=" + technologySet +
                '}';
    }
}
