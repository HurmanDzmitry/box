package selenium;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Wait;

import java.util.concurrent.TimeUnit;

class BasicTest {

    private WebDriver driver;
    private WebElement element;
    private Wait<WebDriver> wait;

    @BeforeEach
    void setUp() {
        System.setProperty("webdriver.chrome.driver", ".\\src\\main\\resources\\chromedriver.exe");
        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.manage().window().maximize();
        driver.get("https://www.amazon.com");
    }

    @Test
    void someDo() {
        String str = driver.getTitle();
        System.out.println(str);
    }

    @AfterEach
    void tearDown() {
        driver.quit();
    }
}