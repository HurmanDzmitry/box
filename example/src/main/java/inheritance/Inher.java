package inheritance;

public class Inher {
    public static void main(String[] args) {
        X a = new X();
        Y b = new Y();
        a.f(a);
        b.f(a);
        a = new Y();
        a.show();
    }
}
 class X {
    void f(Y b) {
        System.out.println("YY");
    }

    void f(X a) {
        System.out.println("YX");
    }

    void show() {
        System.out.println("X.show()");
    }
}

class Y extends X {
    void f(Y b) {
        System.out.println("XY");
    }

    void f(X a) {
        System.out.println("XX");
    }

    void show() {
        System.out.println("Y.show()");
    }
}