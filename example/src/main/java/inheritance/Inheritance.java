package inheritance;

public class Inheritance {
    public static void main(String[] args) {
        A a = new A();
        B b = new B();
        A c = new B();
        System.out.println(a.x);
        System.out.println(a.y);
        a.m1();
        System.out.println("-----------");
        System.out.println(b.x);
        System.out.println(b.y);
        b.m1();
        b.m4();
        b.m3();
        B.m3();
        System.out.println("-----------");
        System.out.println(c.x);
        System.out.println(c.y);
        c.m1();
//        c.m4(); dont work
//        c.m3();
//        c.m3();
    }
}

class A {
    public int x = 1;
    public static int y = 11;

    void m1() {
        System.out.println("A");
    }

    static void m2() {
        System.out.println("SA");
    }
}

class B extends A {
    public int x = 2;
    public static int y = 12;

    void m1() {
        System.out.println("B");
    }

    static void m2() {
        System.out.println("SB");
    }

    static void m3() {
        System.out.println("SX");
    }

    void m4() {
        System.out.println("X");
    }
}