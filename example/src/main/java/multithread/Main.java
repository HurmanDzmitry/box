package multithread;

public class Main {
    public static void main(String[] args) throws InterruptedException {
        A a = new A();
        B b = new B();
        C c = new C();
        a.start();
        Thread th = new Thread(b);
        th.start();
        c.start();
        Thread.sleep(1000);
        System.out.println(Thread.currentThread().getName() + " qwerty");
    }
}


class A extends Thread {
    @Override
    public void run() {
        for (int i = 0; i < 10; i++) {
            System.out.println(Thread.currentThread().getName() + ": " + i);
        }
    }
}


class B implements Runnable {
    @Override
    public void run() {
        for (int i = 0; i < 10; i++) {
            System.out.println(Thread.currentThread().getName() + ": " + i);
        }
    }
}


class C {
    public void start() {
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < 10; i++) {
                    System.out.println(Thread.currentThread().getName() + ": " + i);
                }
            }
        });
        thread.start();
    }
}
