package logger;

import java.util.logging.Level;

public class Logger {
    private static final java.util.logging.Logger LOGGER = java.util.logging.Logger.getLogger(Logger.class.getName());

    public static void main(String[] args) {
        int x = 3;
        int y = 0;
        Integer i = null;
        System.out.println(y / x + 1);
        LOGGER.log(Level.INFO, "Well done\n");
        System.out.println("Well");
        try {
            System.out.println(args[y]);
        } catch (ArrayIndexOutOfBoundsException e) {
            LOGGER.log(Level.WARNING, e.getMessage(), e);
            System.out.println("Fail");
        }
        try {
            System.out.println(x / y);

        } catch (ArithmeticException e) {
            LOGGER.log(Level.WARNING, e.getMessage(), e);
            System.out.println("Fail");
        }
        try {
            System.out.println(i / x);
        } catch (NullPointerException e) {
            LOGGER.log(Level.WARNING, e.getMessage(), e);
            System.out.println("Fail");
        }
    }
}


//java -Djava.util.logging.config.file=logging.properties -cp bin logger.Logger in .bat or configuration without -cp and java
