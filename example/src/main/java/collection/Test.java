package collection;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class Test {
    public static void main(String[] args) {
        List<Integer> list = new ArrayList<>(5);
        System.out.println(list);
        list.add(1);
        list.add(2);
        list.add(3);
        System.out.println(list.size());
        System.out.println(list.get(2));
//        System.out.println(list.get(3));
        list.remove(1);
        System.out.println(list.size());
//        System.out.println(list.get(2));

        LinkedList<Integer> list1 = new LinkedList<>();
        list1.add(1);
        list1.add(2);
        list1.add(3);
    }
}
