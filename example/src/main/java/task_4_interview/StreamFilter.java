package task_4_interview;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

public class StreamFilter {

    //need only even
    public static void main(String[] args) {
        List<Integer> collection = new ArrayList<>();
        collection.add(1);
        collection.add(2);
        collection.add(3);
        collection.add(4);
        collection.add(5);
        System.out.println(filterIterator(collection));
//        System.out.println(filterStream(collection));
    }

    static public Collection<Integer> filterStream(Collection<Integer> collection) {
        return collection.stream().filter(i -> i % 2 == 0).collect(Collectors.toCollection(ArrayList::new));
    }

    static public Collection<Integer> filterIterator(Collection<Integer> collection) {
        Iterator<Integer> it = collection.iterator();
        if (it.hasNext()) {
            if (it.next() % 2 == 0) {
                it.remove();
            }
        }
        ArrayList<Integer> arr = new ArrayList<>();
        it.forEachRemaining(arr::add);
        return arr;
    }
}