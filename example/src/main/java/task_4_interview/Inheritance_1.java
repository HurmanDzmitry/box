package task_4_interview;

public class Inheritance_1 {

    public static void main(String[] args) {
        new A();
    }

    static class A {

        private int a = 1;

        public A() {
            System.out.println(getA());
        }

        public int getA() {
            return a;
        }
    }
}