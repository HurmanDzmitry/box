package task_4_interview;

public class StringPool {

    public static void main(String[] args) {
        String s1 = "A";
        String s2 = "A";
        String s3 = new String("A");
//        s3.intern();
//        s3 = s3.intern();
        String s4 = new String("A");

        System.out.println(s1 == s2);
        System.out.println(s1 == s3);
        System.out.println(s3 == s4);
    }
}
