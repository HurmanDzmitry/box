package task_4_interview;

public class Inheritance_2 {

    public static void main(String[] args) {
        new B();
    }

    static class A {

        private int a = 1;

        public A() {
            System.out.println(getA());
        }

        public int getA() {
            return a;
        }
    }

    static class B extends A {

        private int a = 2;

        public B() {
            System.out.println(getA());
        }

        public int getA() {
            return a;
        }
    }
}