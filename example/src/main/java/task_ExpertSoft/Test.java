package task_ExpertSoft;

import java.util.ArrayList;
import java.util.List;

public class Test {
    public static void main(String[] args) {
        List<Integer> a = new ArrayList<>();
        a.add(1);
        a.add(3);
        a.add(5);
        List<Integer> b = new ArrayList<>();
        b.add(2);
        b.add(6);
        b.add(8);

        merge(a, b);
    }

    private static void merge(List<Integer> a, List<Integer> b) {
        List<Integer> c = new ArrayList<>(a.size() + b.size());
        int i = 0;
        int j = 0;

        while (i < a.size() && j < b.size()) {
            if (a.get(i) <= b.get(j)) {
                c.add(a.get(i));
                i++;
            } else {
                c.add(b.get(j));
                j++;
            }
        }
        while (i < a.size()) {
            c.add(a.get(i));
            i++;
        }
        while (j < b.size()) {
            c.add(b.get(j));
            j++;
        }
        a = c;
        System.out.println(a);
        System.out.println(b);
    }
}
