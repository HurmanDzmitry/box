package task_ExpertSoft;

import java.util.ArrayList;
import java.util.List;
import java.util.TreeSet;

public class Main {
    public static void main(String[] args) {
        List<Integer> a = new ArrayList<>();
        a.add(1);
        a.add(3);
        a.add(5);
        List<Integer> b = new ArrayList<>();
        b.add(1);
        b.add(6);
        b.add(8);
        merge1(a, b);
        merge2(a, b);
    }

    static void merge1(List<Integer> a, List<Integer> b) {
        a.addAll(b);
        for (int i = a.size() - 1; i > 0; i--) {
            for (int j = 0; j < i; j++) {
                if (a.get(j) > a.get(j + 1)) {
                    Integer tmp = a.get(j);
                    a.add(j, a.get(j + 1));
                    a.remove(j + 1);
                    a.add(j + 1, tmp);
                    a.remove(j + 2);
                }
            }
        }
        System.out.println(a);
        System.out.println(b);
    }

    static void merge2(List<Integer> a, List<Integer> b) {
        /*если не нужны дубликаты*/
        a.addAll(b);
        TreeSet<Integer> x = new TreeSet<>(a);
        a.clear();
        a.addAll(x);
        System.out.println(a);
        System.out.println(b);
    }
}