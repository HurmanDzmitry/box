package insatnce;

public class Test {

    public static void main(String[] args) {
        Integer i = 9;
        System.out.println(i instanceof Number);
        System.out.println(i instanceof Object);
        System.out.println(i instanceof Integer);
        System.out.println(i instanceof Comparable);
//        System.out.println(i instanceof String);

        System.out.println("---------");

        A a = new A();
        B b = new B();
        A c = new B();
        System.out.println(a instanceof A);
        System.out.println(a instanceof B);
        System.out.println(b instanceof A);
        System.out.println(b instanceof B);
        System.out.println(c instanceof A);
        System.out.println(c instanceof B);

        System.out.println("---------");

        B b1 = new C();
        A a1 = b1;
        System.out.println(a1 instanceof A);
        System.out.println(a1 instanceof B);
        System.out.println(a1 instanceof C);
        System.out.println(b1 instanceof A);
        System.out.println(b1 instanceof B);
        System.out.println(b1 instanceof C);

        System.out.println("---------");

        D<String> d = new D<>();
        System.out.println(d instanceof D);
//        System.out.println(d instanceof D<String>);
    }
}

class A {
}

class B extends A {
}

class C extends B {
}

class D<T> {
}