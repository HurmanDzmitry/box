package task_Syberry;

public class TestAssignment {
    public static int[][] matrixGeneration(int n) {
//        конечно лучше все ограничения вынести в константы,
//        но есть конкретный шаблон без полей
        if (n <= 0 || n >= 10_000) {
            throw new IllegalArgumentException("Incorrect value: n");
        }

        int[][] matrix = new int[n][n];
        int min = 0;
        int max = 100;

        for (int[] arr : matrix) {
            for (int j = 0; j < n; j++) {
                arr[j] = min + (int) (Math.random() * max);
            }
        }
        return matrix;
    }

    public static int findLargest(int n, int m) {
        if (m <= 0 || m > n) {
            throw new IllegalArgumentException("Incorrect value: m");
        }

//        генерирование матрицы
        int[][] matrix = matrixGeneration(n);

//        нахождение максимального элемента
        int max = Integer.MIN_VALUE;
        for (int[] arr : matrix) {
            for (int i : arr) {
                if (i > max) {
                    max = i;
                }
            }
            if (max == 99) {
                break;
            }
        }

//        получение подматриц
//        надеюсь правильно понял понятие подматриц
//        пример:
//        findLargest(3, 1)
//        допустим генерировалось:
//        1 1 1
//        1 1 1
//        1 1 1
//        9 подматриц
//        результат метода= 9?
//
//        findLargest(3, 2)
//        допустим генерировалось :
//        1 2 1
//        1 1 1
//        1 1 2
//        4 подматрицы
//        результат метода= 3?
//
//        findLargest(3, 2)
//        допустим генерировалось :
//        1 1 1
//        1 2 1
//        1 1 1
//        4 подматриц
//        результат метода= 4?
        int count = 0;
        int[] sample = new int[m * m];
        for (int i = 0; i < n - m + 1; i++) {
            for (int j = 0; j < n - m + 1; j++) {
                for (int k = 0; k < m; k++) {
                    System.arraycopy(matrix[i + k], j, sample, m * k, m);
                }
//                проверка на наличие максимального элемента
                for (int x : sample) {
                    if (x == max) {
                        count++;
                        break;
                    }
                }
            }
        }
        return count;
    }

    public static void simulation(int n, int m, double p, int trials) {
        if (p < 0 || p >= 1) {
            throw new IllegalArgumentException("Incorrect value: p");
        }
        if (trials < 100 || trials > 1_000_000) {
            throw new IllegalArgumentException("Incorrect value: trials");
        }

//        надеюсь я правильно понял, что процент выборки это отнощение
//        результата findLargest(int n, int m) и количества возможных подматриц
        double possible = Math.pow(n - m + 1, 2);
        int count = 0;
        for (int i = 0; i < trials; i++) {
            int x = findLargest(n, m);
            if (x / possible < p) {
                count++;
            }
        }
        System.out.print("P = ");
        System.out.printf("%.2f", 1.0 * count / trials);
    }
}
