package task_AppShire;

import java.util.Arrays;

public class Nonogram {

    public static int[][] solve(int[][][] clues) {
        // init
        int[][] answer = new int[5][5];
        // fill default values
        for (int[] Line : answer) {
            Arrays.fill(Line, -1);
        }
        //loop
        for (int i = 0; i < 10; i++) {
            // vert
            for (int j = 0; j < answer[0].length; j++) {
                int[] column = getColumn(answer, j);
                int[] clue = getVertClue(clues, j);
                checkLine(clue, column);
                setNonogramColumn(answer, column, j);
            }
            //hor
            for (int j = 0; j < answer.length; j++) {
                int[] line = getLine(answer, j);
                int[] clue = getHorClue(clues, j);
                checkLine(clue, line);
                setNonogramLine(answer, line, j);
            }
        }
        return answer;
    }

    private static int[] getVertClue(int[][][] clues, int columnIndex) {
        return clues[0][columnIndex];
    }

    private static int[] getHorClue(int[][][] clues, int lineIndex) {
        return clues[1][lineIndex];
    }

    private static int[] getColumn(int[][] nonogram, int columnIndex) {
        int[] column = new int[5];
        for (int i = 0; i < nonogram.length; i++) {
            column[i] = nonogram[i][columnIndex];
        }
        return column;
    }

    private static int[] getLine(int[][] nonogram, int lineIndex) {
        return nonogram[lineIndex];
    }

    private static void setNonogramColumn(int[][] nonogram, int[] newColumn, int columnIndex) {
        for (int i = 0; i < nonogram.length; i++) {
            nonogram[i][columnIndex] = newColumn[i];
        }
    }

    private static void setNonogramLine(int[][] nonogram, int[] newLine, int lineIndex) {
        nonogram[lineIndex] = newLine;
    }

    private static void checkLine(int[] clue, int[] line) {
        if (Arrays.stream(line).noneMatch(i -> i == -1)) {
            return;
        }

        //fill at start
        int clueSum = Arrays.stream(clue).sum() + clue.length - 1;
        if (clueSum > 2) {
            int chainSum = 0;
            for (int clueValue : clue) {
                chainSum += clueValue;
                int startIndex = 5 - (clueSum - chainSum + clueValue);
                int endIndex = chainSum;
                if (endIndex >= startIndex) {
                    Arrays.fill(line, startIndex, endIndex, 1);
                }
                chainSum++;
            }
        }

        //full in progress -> 1
        if (5 - countOfZero(line) == Arrays.stream(clue).sum()) {
            for (int i = 0; i < line.length; i++) {
                if (line[i] < 0) {
                    line[i] = 1;
                }
            }
            return;
        }

        //full in progress -> 0
        if (countOfPositive(line) == Arrays.stream(clue).sum()) {
            for (int i = 0; i < line.length; i++) {
                if (line[i] < 1) {
                    line[i] = 0;
                }
            }
            return;
        }

        //partially in progress -> 0
        if (Arrays.stream(clue).allMatch(i -> i == 1)) {
            for (int i = 0; i < line.length; i++) {
                if (line[i] == 1) {
                    if (i - 1 >= 0) {
                        line[i - 1] = 0;
                    }
                    if (i + 1 < line.length) {
                        line[i + 1] = 0;
                    }
                }
            }
        }
        if (clue.length == 1 && countOfPositive(line) > 0) {
            int startIndex = -1;
            int endIndex = -1;
            for (int i = 0; i < line.length; i++) {
                if (line[i] == 1 && startIndex == -1) {
                    startIndex = i;
                    endIndex = i;
                }
                if (line[i] == 1 && startIndex > -1) {
                    endIndex = i;
                }
            }
            int x = endIndex - clue[0];
            if (x >= 0) {
                Arrays.fill(line, 0, endIndex - clue[0], 0);
            }
            int y = startIndex + clue[0];
            if (y < line.length) {
                Arrays.fill(line, y, 5, 0);
            }
        }

        //partially in progress -> 1
        if (clue.length == 1 & clue[0] > 1) {
            int[][] possibleSections = getPossibleSections(line);
            int[][] validSections = Arrays.stream(possibleSections).filter(i -> i[1] - i[0] + 1 >= clue[0]).toArray(int[][]::new);
            if (validSections.length == 1 && clue[0] * 2 > validSections[0][1] - validSections[0][0] + 1) {
                Arrays.fill(line, validSections[0][1] - clue[0] + 1, validSections[0][0] + clue[0], 1);
            }
        }

        //divide by sections
        if (countOfPossibleSections(line) == clue.length) {
            int[][] possibleSections = getPossibleSections(line);
            for (int i = 0; i < clue.length; i++) {
                if (clue[i] == possibleSections[i][1] - possibleSections[i][0] + 1) {
                    Arrays.fill(line, possibleSections[i][0], possibleSections[i][1] + 1, 1);
                    continue;
                }
                if (clue[i] * 2 > possibleSections[i][1] - possibleSections[i][0] + 1) {
                    Arrays.fill(line, possibleSections[i][1] - clue[i] + 1, possibleSections[i][0] + clue[i], 1);
                }
            }
        }
    }

    private static int[][] getPossibleSections(int[] line) {
        int[][] sections = new int[countOfPossibleSections(line)][2];
        int sectionIndex = 0;
        int startIndex = -1;
        int finishIndex = -1;

        for (int i = 0; i < line.length; i++) {
            //new section
            if (line[i] != 0 && (i == 0 || line[i - 1] == 0)) {
                startIndex = i;
                finishIndex = startIndex;
                continue;
            }
            //continue section
            if (line[i] != 0 && i - 1 == finishIndex) {
                finishIndex++;
                continue;
            }
            //skip cell
            if (i > 0 && line[i] == 0 && line[i - 1] == 0) {
                finishIndex++;
                continue;
            }
            //end section
            if (line[i] == 0 && finishIndex > 0) {
                finishIndex = i - 1;
                sections[sectionIndex] = new int[]{startIndex, finishIndex};
                sectionIndex++;
            }

        }
        //last end
        if (line[line.length - 1] != 0) {
            sections[sectionIndex] = new int[]{startIndex, finishIndex};
        }
        return sections;
    }

    private static int countOfPossibleSections(int[] line) {
        int section = 0;
        if (line[0] != 0) {
            section++;
        }
        for (int i = 1; i < line.length; i++) {
            if (line[i] != 0 && line[i - 1] == 0) {
                section++;
            }
        }
        return section;
    }

    private static int countOfNegative(int[] line) {
        return (int) Arrays.stream(line).filter(i -> i == -1).count();
    }

    private static int countOfPositive(int[] line) {
        return (int) Arrays.stream(line).filter(i -> i == 1).count();
    }

    private static int countOfZero(int[] line) {
        return (int) Arrays.stream(line).filter(i -> i == 0).count();
    }

    public static void main(String[] args) {
//        int[][][] clue = new int[][][]
//                {{{1, 1}, {4}, {1, 1, 1}, {3}, {1}},
//                        {{1}, {2}, {3}, {2, 1}, {4}}};
//        int[][] answer = new int[][]
//                {{0, 0, 1, 0, 0},
//                        {1, 1, 0, 0, 0},
//                        {0, 1, 1, 1, 0},
//                        {1, 1, 0, 1, 0},
//                        {0, 1, 1, 1, 1}};

//        int[][][] clue = new int[][][]
//                {{{1}, {3}, {1}, {3, 1}, {3, 1}},
//                        {{3}, {2}, {2, 2}, {1}, {1, 2}}};
//        int[][] answer = new int[][]
//                {{0, 0, 1, 1, 1},
//                        {0, 0, 0, 1, 1},
//                        {1, 1, 0, 1, 1},
//                        {0, 1, 0, 0, 0},
//                        {0, 1, 0, 1, 1}};

        int[][][] clue = new int[][][]
//                {{{2}, {3}, {4}, {1, 1}, {1, 1}},
//                        {{2}, {3, 1}, {3}, {1, 1}, {2}}};
//
                {{{1}, {2}, {3}, {1, 3}, {3}}, {{1, 1}, {3}, {3}, {3}, {2}}};

//        int[][] answer = new int[][]
//                {{1, 1, 0, 0, 0},
//            {1, 1, 1, 0, 1},
//            {0, 1, 1, 1, 0},
//            {0, 0, 1, 0, 1},
//            {0, 0, 1, 1, 0}};

//        int[] line = new int[]{-1, -1, 0, 0, -1};
//        for (int i = 0; i < 10; i++) {
//            checkLine(new int[]{2}, line);
//        }
//        System.out.println(Arrays.toString(line));
//        System.out.println(Arrays.deepToString(getPossibleSections(line)));

        Arrays.stream(solve(clue)).forEach(i -> System.out.println(Arrays.toString(i)));

    }
}