package task_AppShire;

import java.util.Arrays;

public class StripComments {

    public static String stripComments(String text, String[] commentSymbols) {
        System.out.println(text);
        System.out.println(Arrays.toString(commentSymbols));


        String[] strings = text.split("\\n");
        for (int i = 0; i < strings.length; i++) {
            for (String symbol : commentSymbols) {
                String line = strings[i];
                int index = line.indexOf(symbol);
                if (index >= 0) {
                    line = line.substring(0, index);
                }
                strings[i] = line.replaceAll("\\s+$", "");
            }
        }
        StringBuilder stringBuilder = new StringBuilder(strings[0]);
        for (int i = 1; i < strings.length; i++) {
            String line = strings[i];
            stringBuilder.append("\n").append(line);
        }
        return stringBuilder.toString();
    }
}