package task_AppShire;

public class SpinWords {

    public String spinWords(String sentence) {
        String[] array = sentence.split(" ");
        for (int i = 0; i < array.length; i++) {
            String s = array[i];
            if (s.length() >= 5) {
                array[i] = reverse(s);
            }
        }
        return String.join(" ", array);
    }

    private String reverse(String string) {
        StringBuilder sb = new StringBuilder(string);
        sb.reverse();
        return sb.toString();
    }
}
