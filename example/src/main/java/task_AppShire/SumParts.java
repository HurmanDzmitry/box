package task_AppShire;

import java.util.Arrays;

class SumParts {

    public static int[] sumParts(int[] ls) {
        int length = ls.length;
        int[] arr = new int[length + 1];
        arr[length] = 0;
        for (int i = 1; i < length + 1; i++) {
            arr[length - i] = ls[length - i] + arr[length - i + 1];
        }
        return arr;
    }
}