package task_AppShire;

public class Dinglemouse {
    public static String shutTheGate(final String farm) {
        char[] chars = farm.toCharArray();
        for (int i = 0; i < chars.length; i++) {
            char element = chars[i];
            boolean isRunAwayLeft = true;
            boolean isRunAwayRight = true;
            if (element == 'H' || element == 'C' || element == 'R') {
                // <-
                for (int j = i - 1; j >= 0; j--) {
                    if (chars[j] == '|') {
                        isRunAwayLeft = false;
                        break;
                    }
                    chars[j] = eat(element, chars[j]);
                }
                // ->
                for (int j = i + 1; j < chars.length; j++) {
                    if (chars[j] == '|') {
                        isRunAwayRight = false;
                        break;
                    }
                    chars[j] = eat(element, chars[j]);
                }

                // run away
                if (isRunAwayLeft || isRunAwayRight) {
                    chars[i] = '.';
                    // eat again <-
                    for (int j = chars.length - 1; j > i; j--) {
                        if (chars[j] == '|') {
                            break;
                        }
                        chars[j] = eat(element, chars[j]);
                    }
                    // eat again ->
                    for (int j = 0; j < i; j++) {
                        if (chars[j] == '|') {
                            break;
                        }
                        chars[j] = eat(element, chars[j]);
                    }
                }
            }
        }
        return String.valueOf(chars);
    }

    private static char eat(char animal, char current) {
        if ((animal == 'H' && (current == 'A' || current == 'V'))
                || (animal == 'R' && current == 'V')) {
            current = '.';
        }
        return current;
    }

    public static void main(String[] args) {
        final String before = "|CCHHHHHHH|RRRRRRRRA";
//        final String before = "|..HH....\\AAAA\\CC..|AAA/VVV/RRRR|CCC";
        final String expected = "|..HH....\\....\\CC..|AAA/.../RRRR|...";
        String answer = shutTheGate(before);
        System.out.println(answer);
        System.out.println(answer.equals(expected));
    }
}