package task_AppShire;

import java.util.ArrayList;
import java.util.List;

public class Josephus {
    public static <T> List<T> josephusPermutation(final List<T> items, final int k) {
        List<T> out = new ArrayList<>();
        int pos = -1;
        int length = items.size();
        while (items.size() > 0) {
            pos = pos + k;
            while (pos > length - 1) {
                pos = pos - length;
            }
            out.add(items.get(pos));
            items.remove(pos);
            pos--;
            length = items.size();
        }
        return out;
    }
}
