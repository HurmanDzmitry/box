package task_AppShire;

import java.util.Arrays;

public class Snail {

    public static int[] snail(int[][] array) {
        int width = array.length;
        int height = array[0].length;
        int[] out = new int[width * height];
        if (out.length == 0) {
            return out;
        }
        System.arraycopy(array[0], 0, out, 0, width);
        Arrays.fill(array[0], -1);
        int counter = height;
        int x = 0;
        int y = height - 1;
        while (out[out.length - 1] == 0) {
            while (x + 1 < width && array[x + 1][y] != -1) {
                out[counter] = array[x + 1][y];
                counter++;
                array[x + 1][y] = -1;
                x += 1;
            }
            while (y - 1 >= 0 && array[x][y - 1] != -1) {
                out[counter] = array[x][y - 1];
                counter++;
                array[x][y - 1] = -1;
                y -= 1;
            }
            while (x - 1 >= 0 && array[x - 1][y] != -1) {
                out[counter] = array[x - 1][y];
                counter++;
                array[x - 1][y] = -1;
                x -= 1;
            }
            while (y + 1 < height && array[x][y + 1] != -1) {
                out[counter] = array[x][y + 1];
                counter++;
                array[x][y + 1] = -1;
                y += 1;
            }
        }
        return out;
    }
}
