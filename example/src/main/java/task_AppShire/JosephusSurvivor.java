package task_AppShire;

import java.util.ArrayList;
import java.util.List;

public class JosephusSurvivor {
    public static int josephusSurvivor(final int n, final int k) {
        List<Integer> items = new ArrayList<>();
        for (int i = 1; i < n + 1; i++) {
            items.add(i);
        }
        int pos = -1;
        int length = items.size();
        while (items.size() > 1) {
            pos = pos + k;
            while (pos > length - 1) {
                pos = pos - length;
            }
            items.remove(pos);
            pos--;
            length = items.size();
        }
        return items.get(0);
    }
}
