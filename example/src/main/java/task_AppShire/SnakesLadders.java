package task_AppShire;

import java.util.Map;

public class SnakesLadders {

    public static final String winMsgTemplate = "Player %s Wins!";
    public static final String loseMsg = "Game over!";
    public static final String currentPositionMsgTemplate = "Player %s is on square %s";

    public static final Map<Integer, Integer> ladders = Map.ofEntries(
            Map.entry(2, 38), Map.entry(7, 14), Map.entry(8, 31),
            Map.entry(15, 26), Map.entry(21, 42), Map.entry(28, 84),
            Map.entry(36, 44), Map.entry(51, 67), Map.entry(71, 91),
            Map.entry(78, 98), Map.entry(87, 94));
    public static final Map<Integer, Integer> snakes = Map.ofEntries(
            Map.entry(16, 6), Map.entry(46, 25), Map.entry(49, 11),
            Map.entry(62, 19), Map.entry(64, 60), Map.entry(74, 53),
            Map.entry(89, 68), Map.entry(92, 88), Map.entry(95, 75),
            Map.entry(99, 80));

    private int current1;
    private int current2;
    private boolean isFirstPlayer = true;

    public String play(int die1, int die2) {
        if ((isFirstPlayer ? current2 : current1) == 100) {
            return loseMsg;
        }

        int current = isFirstPlayer ? current1 : current2;
        current += die1 + die2;
        current = ladders.getOrDefault(current, current);
        current = snakes.getOrDefault(current, current);
        while (current > 100) {
            current = 100 - (current - 100);
            current = ladders.getOrDefault(current, current);
            current = snakes.getOrDefault(current, current);
        }

        if (isFirstPlayer) {
            current1 = current;
        } else {
            current2 = current;
        }

        String msg;
        if (current == 100) {
            msg = String.format(winMsgTemplate, isFirstPlayer ? 1 : 2);
        } else {
            msg = String.format(currentPositionMsgTemplate, isFirstPlayer ? 1 : 2, current);
        }

        if (die1 != die2) {
            isFirstPlayer = !isFirstPlayer;
        }

        return msg;
    }
}