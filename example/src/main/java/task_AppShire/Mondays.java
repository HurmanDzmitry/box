package task_AppShire;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

public class Mondays {
    static long count(final LocalDate birthday, final LocalDate today) {
        LocalDate startWork = birthday.plusYears(22);
        LocalDate endWork = birthday.plusYears(78);
        if (startWork.isAfter(today)) {
            return 0;
        }
        if (endWork.isAfter(today)) {
            endWork = today;
        } else {
            endWork = endWork.minusDays(1);
        }
        DayOfWeek dayOfWeekToStartWork = startWork.getDayOfWeek();
        long days = ChronoUnit.DAYS.between(startWork, endWork) + 1;
        long remainder = days % 7;
        long mondays = (days - remainder) / 7;
        if ((dayOfWeekToStartWork.getValue() + remainder) > 8
                || (dayOfWeekToStartWork == DayOfWeek.MONDAY && remainder > 0)) {
            ++mondays;
        }
        return mondays;
    }
}