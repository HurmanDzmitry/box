package lambda_expression;

public class Collect {
    public static void main(String[] args) {
        Operation operation;
        operation = (a, b) -> (a + b);
        System.out.println(operation.calculate(10, 10));

        operation = (a, b) -> (a - b);
        System.out.println(operation.calculate(20, 20));

        operation = (a, b) -> (sum(a, b));
        System.out.println(operation.calculate(5, 5));

        operation = Collect::sum;
        System.out.println(operation.calculate(2, 2));

        X x = new X();
        operation = (a, b) -> (x.difference(a, b));
        System.out.println(operation.calculate(11, 1));

        operation = x::difference;
        System.out.println(operation.calculate(11, 1));

        operation = (a, b) -> (x.difference(a));
        System.out.println(operation.calculate(1, 100));

//        x = null;     /*must be final!*/
    }

    static int sum(int x, int y) {
        return x + y;
    }
}

interface Operation {
    int calculate(int a, int b);
}

class X {
    X() {
    }

    int difference(int x, int y) {
        return x - y;
    }

    int difference(int x) {
        return --x;
    }

}
