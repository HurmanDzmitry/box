package lambda_expression;

public class IntRunnable {
    public static void main(String[] args) {

//        old
        new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println("Hello from thread1");
            }
        }).start();


//        new
        new Thread(() -> System.out.println("Hello from thread2")).start();


        System.out.println("qwerty");
    }
}
