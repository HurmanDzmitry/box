package memory;

public class JavaHeapVerboseGCTest {
    private static long x = Runtime.getRuntime().freeMemory();

    private static int capacity = 1_000_000_000;
    //    private static List<Integer> list = new ArrayList<>(capacity);
    private static byte[] list = new byte[capacity];

    public static void main(String[] args) {
        System.out.println(x);

        final int MegaBytes = 10241024;
        long freeMemory = Runtime.getRuntime().freeMemory() / MegaBytes;
        long totalMemory = Runtime.getRuntime().totalMemory() / MegaBytes;
        long maxMemory = Runtime.getRuntime().maxMemory() / MegaBytes;

        System.out.println("JVM freeMemory: " + freeMemory);
        System.out.println("JVM totalMemory also equals to initial heap size of JVM : "
                + totalMemory);
        System.out.println("JVM maxMemory also equals to maximum heap size of JVM: "
                + maxMemory);


        System.out.println(Runtime.getRuntime().freeMemory());

        for (int i = 0; i < capacity; i++) {
//            list.add(i);
            list[i] = 1;
        }

//        System.out.println("size: " + list.size());
        System.out.println("size: " + list.length);
        System.out.println(Runtime.getRuntime().freeMemory());
        System.gc();
        list = null;

//        for (int i = 0; i < capacity - i; i++) {
//            list.remove(i);
//        }

//        System.out.println("size: " + list.size());
        System.out.println(Runtime.getRuntime().freeMemory());
//        System.gc();
//        System.out.println(Runtime.getRuntime().freeMemory());
    }
}
