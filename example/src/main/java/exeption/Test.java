package exeption;

public class Test {
    public static void main(String[] args) {
        try {
            method();
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("ArithmeticException!!!!");
        }
        System.out.println("Next message!");
    }

    private static void method() throws Exception {
        int x = 5;
        int y = 0;
        try {
            int c = x / y;
        } catch (ArithmeticException e) {
            throw new Exception("My exc", e);
//            don't work after
//            e.printStackTrace();
//            System.out.println("ArithmeticException!!!!");
        } finally {
            System.out.println("Finally block");
        }
    }
}

//ничего не работает после return или throw
//если return и в try и finally то выполниться в finally
