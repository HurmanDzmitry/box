package task_from_interview_Todes;

import java.util.ArrayList;
import java.util.List;

//что допустимо?

public class ListInit {
    public static void main(String[] args) {

//        List<Integer> list1 = new List<Integer>();
        List<Integer> list2 = new ArrayList<Integer>();
//        List<Number> list3 = new ArrayList<Integer>();
//        List<Integer> list4 = new ArrayList<Number>();
        List<?> list5 = new ArrayList<Integer>(list2);
        List<Integer> list6 = new ArrayList<>();
    }
}
