package task_from_interview_Todes;

//1.Правильно ли написан класс Parent.
//2.Какие методы можно переопределить в классе Child

public class Parent {
    private int inner;

    public Parent() {
    }

    private void priM() {
        System.out.println("pri");
    }

    protected void proM() {
        System.out.println("pro");
    }

    public void pubM() {
        System.out.println("pub");
    }

//    abstract void aM();

    public int getInner() {
        return inner;
    }

//    public int getInner() {
//        return inner;
//    }

    public void setInner(int inner) {
        this.inner = inner;
    }
}

class Child extends Parent {

}