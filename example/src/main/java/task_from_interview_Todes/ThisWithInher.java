package task_from_interview_Todes;

//что выведется на консоль?

public class ThisWithInher extends B {
    public static void main(String[] args) {
        new ThisWithInher();
    }
}

class B extends A {
    public B() {
        super("a");
    }
}

class A {
    private String str;

    public A(String str) {
        System.out.println(this.getClass());
        this.str = str;
    }
}
