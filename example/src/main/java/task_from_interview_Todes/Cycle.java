package task_from_interview_Todes;

import java.util.ArrayList;
import java.util.Arrays;

public class Cycle {
    public static void main(String[] args) {
        ArrayList<String> list = new ArrayList<>(Arrays.asList("a", "b", "c", "d"));

//        1
//        for (int i = 0; i < list.size(); i++) {
//            list.remove(i);
//        }

//        2
//        for (int i = 0; i < list.size(); i++) {
//            if (list.get(i).equals("b") || list.get(i).equals("c")) {
//                list.remove(i);
//            }
//        }

//        3
        for (String s : list) {
            if (s.equals("b")) {
                list.remove(s);
            }
        }

        System.out.println(list);
    }
}
