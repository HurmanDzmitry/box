package task_TouchSoft;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public class Task {
    public static void main(String[] args) {
        print(validate("{}}"));
    }

    static Set<String> validate(String str) {
        Set<String> box = new HashSet<>(Collections.emptySet());

        int locks = 0, unlocks = 0;
        for (char element : str.toCharArray()) {
            if (element == '{')
                locks++;
            if (element == '}')
                unlocks++;
        }

        char mustBeRemoved = ' ';
        if (locks == unlocks && validCheck(str))
            box.add(str);
        else {
            if (locks > unlocks)
                mustBeRemoved = '{';
            else
                mustBeRemoved = '}';
        }


        int[] arr = new int[Math.abs(locks - unlocks) + 1];
        int[] array;
        Set<int[]> indexOfRemovedChar = new HashSet<>(Collections.emptySet());
        int helper = 0;
        for (int i = 0; i < str.length(); i++) {
            if (str.charAt(i) == mustBeRemoved) {
                arr[helper] = str.indexOf(mustBeRemoved, i);
                helper++;
            }
        }
        int count = fuctorial(arr.length);
        int max = arr.length - 1;
        int shift = max;
        int t;
        while (count > 0) {
            t = arr[shift];
            arr[shift] = arr[shift - 1];
            arr[shift - 1] = t;

            int countOfluck = 0;
            array = Arrays.copyOf(arr, arr.length - 1);
            for (int i = 0; i < array.length - 1; i++) {
                if (array[i] < array[i + 1]) {
                    countOfluck++;
                }
                if (countOfluck == array.length - 1) {
                    indexOfRemovedChar.add(array);
                }
            }

            count--;
            if (shift < 2) {
                shift = max;
            } else {
                shift--;
            }
        }

        int removeNumber;
        String strForWork;
        for (int[] x : indexOfRemovedChar) {
            strForWork = str;
            removeNumber = Math.abs(locks - unlocks);
            for (int i = 0; i < x.length; i++) {
                if (removeNumber > 0) {
                    strForWork = strForWork.substring(0, x[i]) + strForWork.substring(x[i] + 1);
                    removeNumber--;
                    if ((i + 1) < x.length) {
                        x[i + 1]--;
                    }
                    if (removeNumber == 0 && validCheck(strForWork)) {
                        box.add(strForWork);
                    }
                }
            }
        }
        return box;
    }

    static boolean validCheck(String str) {
        StringBuilder builder = new StringBuilder(str);
        builder.reverse();
        int validLocks = 0, validUnlocks = 0, validCount = 0;
        if (str.indexOf('{') < str.indexOf('}') && builder.toString().indexOf('{') > builder.toString().indexOf('}')) {
            for (char element : str.toCharArray()) {
                if (element == '{')
                    validLocks++;
                if (element == '}')
                    validUnlocks++;
                if (validLocks >= validUnlocks)
                    validCount++;
            }
        }
        return validCount == str.length();
    }

    static int fuctorial(int n) {
        return (n > 0) ? n * fuctorial(n - 1) : 1;
    }

    static void print(Set<String> set) {
        for (String s : set) {
            System.out.println(s);
        }
    }
}