package implement;

class C implements A, B {
    @Override
    public void getName() {
        System.out.println("Super");
    }

    @Override
    public void getName1() {
        A.super.getName1();
    }
}

interface A {
    int x = 35;

    static void getInfo() {
        System.out.println("Info");
    }

    default void getName1() {
        System.out.println(getClass().getSimpleName() + 13);
    }
}

interface B {
    void getName();

    default void getName1() {
        System.out.println(getClass().getSimpleName() + 7);
    }
}

public class InterfaceTest {
    static C c = new C();

    public static void main(String[] args) {
        c.getName();
        c.getName1();
        A.getInfo();
        System.out.println(A.x);
    }
}