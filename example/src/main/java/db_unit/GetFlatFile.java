//package db_unit;
//
//import org.dbunit.database.DatabaseConnection;
//import org.dbunit.database.IDatabaseConnection;
//import org.dbunit.database.QueryDataSet;
//import org.dbunit.dataset.IDataSet;
//import org.dbunit.dataset.ITable;
//import org.dbunit.dataset.xml.FlatXmlDataSet;
//import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
//
//import java.io.FileInputStream;
//import java.io.FileOutputStream;
//import java.sql.Connection;
//import java.sql.DriverManager;
//
//public class GetFlatFile {
//    public static void main(String[] args) throws Exception {
//
//        Connection jdbcConnection = DriverManager.getConnection(
//                "jdbc:mysql://localhost:3306/library_test?serverTimezone=Europe/Minsk&useUnicode=true&useSSL=false&allowPublicKeyRetrieval=true",
//                "root",
//                "1234");
//
//        IDatabaseConnection connection = new DatabaseConnection(jdbcConnection/*,"LIBRARY_TEST"*/);
////        connection.getConfig().setProperty(DatabaseConfig.FEATURE_QUALIFIED_TABLE_NAMES, true);
//        QueryDataSet partialDataSet = new QueryDataSet(connection);
//        partialDataSet.addTable("authors_test");
////        partialDataSet.addTable("BAR", + can sql);
//        FlatXmlDataSet.write(partialDataSet, new FileOutputStream("by/epam/javatr/dao/db_unit/data.xml"));
//
//
//        IDataSet fileDataSet = new FlatXmlDataSetBuilder().build(new FileInputStream("by/epam/javatr/dao/db_unit/data.xml"));
////        DataFileLoader loader = new FlatXmlDataFileLoader();
////        IDataSet databaseDataSet =
////                loader.load("/data.xml");
//        ITable table = fileDataSet.getTable("authors_test");
//
//        for (int i = 0; i < table.getRowCount(); i++) {
//            System.out.println(table.getValue(i, "name_author"));
//        }
//
//        QueryDataSet databaseDataSet = new QueryDataSet(connection);
//        databaseDataSet.addTable("authors_test");
////        IDataSet databaseDataSet = connection.createDataSet();
//        table = databaseDataSet.getTable("authors_test");
//
//        for (int i = 0; i < table.getRowCount(); i++) {
//            System.out.println(table.getValue(i, "name_author"));
//        }
//    }
//}