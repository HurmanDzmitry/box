//package db_unit;
//
//import org.dbunit.IDatabaseTester;
//import org.dbunit.JdbcDatabaseTester;
//import org.dbunit.dataset.IDataSet;
//import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
//import org.dbunit.operation.DatabaseOperation;
//import org.junit.*;
//import org.junit.runner.RunWith;
//import org.junit.runners.JUnit4;
//
//import java.io.InputStream;
//import java.sql.Connection;
//import java.sql.ResultSet;
//
//@RunWith(JUnit4.class)
//public class OldSchoolDbUnitTest {
//
//    private static IDatabaseTester tester = null;
//
//    private Connection connection;
//
//    @BeforeClass
//    public static void setUp() throws Exception {
//        tester = initDatabaseTester();
//    }
//
//    private static IDatabaseTester initDatabaseTester() throws Exception {
//        JdbcDatabaseTester tester
//                = new JdbcDatabaseTester(
//                "com.mysql.cj.jdbc.Driver",
//                "jdbc:mysql://localhost:3306/library_test?serverTimezone=Europe/Minsk&useUnicode=true&useSSL=false&allowPublicKeyRetrieval=true",
//                "root",
//                "1234");
//
//        tester.setDataSet(initDataSet());
//        tester.setSetUpOperation(DatabaseOperation.REFRESH);
//        tester.setTearDownOperation(DatabaseOperation.DELETE_ALL);
//        return tester;
//    }
//
//    private static IDataSet initDataSet() throws Exception {
//        try (InputStream is = OldSchoolDbUnitTest.class.getClassLoader().getResourceAsStream("by/epam/javatr/dao/db_unit/data.xml")) {
//            return new FlatXmlDataSetBuilder().build(is);
//        }
//    }
//
//    @Before
//    public void setup() throws Exception {
////        tester.onSetup();
//        connection = tester.getConnection().getConnection();
//    }
//
//    @After
//    public void tearDown() throws Exception {
////        tester.onTearDown();
//    }
//
//    @Test
//    public void test_1() throws Exception {
//        ResultSet rs = connection.createStatement().executeQuery("select * from authors");
//        while (rs.next()) {
//            System.out.println(rs.getString(2));
//        }
//        Assert.assertTrue(rs.next());
//        Assert.assertEquals(rs.getString(2), ("Дэн Браун"));
//    }
//}
