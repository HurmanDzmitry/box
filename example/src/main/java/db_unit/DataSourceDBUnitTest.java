//package db_unit;
//
//import com.mysql.cj.jdbc.MysqlDataSource;
//import org.dbunit.DataSourceBasedDBTestCase;
//import org.dbunit.dataset.IDataSet;
//import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
//import org.dbunit.operation.DatabaseOperation;
//import org.junit.After;
//import org.junit.Before;
//import org.junit.Test;
//
//import javax.sql.DataSource;
//import java.sql.Connection;
//import java.sql.ResultSet;
//
//
//public class DataSourceDBUnitTest extends DataSourceBasedDBTestCase {
//
//    private Connection connection;
//
//
//    @Override
//    protected DataSource getDataSource() {
//        MysqlDataSource dataSource = new MysqlDataSource();
////        JdbcDataSource dataSource = new JdbcDataSource();
//        dataSource.setURL(
//                "jdbc:mysql://localhost:3306/library_test?serverTimezone=Europe/Minsk&useUnicode=true&useSSL=false&allowPublicKeyRetrieval=true");
//        dataSource.setUser("root");
//        dataSource.setPassword("1234");
//        return dataSource;
//    }
//
//    @Override
//    protected IDataSet getDataSet() throws Exception {
//        return new FlatXmlDataSetBuilder().build(getClass().getClassLoader()
//                .getResourceAsStream("by/epam/javatr/dao/db_unit/data.xml"));
//    }
//
//    @Override
//    protected DatabaseOperation getSetUpOperation() {
//        return DatabaseOperation.REFRESH;
//    }
//
//    @Override
//    protected DatabaseOperation getTearDownOperation() {
//        return DatabaseOperation.DELETE_ALL;
//    }
//
//    @Before
//    public void setUp() throws Exception {
//        super.setUp();
//        connection = getConnection().getConnection();
//    }
//
//    @After
//    public void tearDown() throws Exception {
//        super.tearDown();
//    }
//
//    @Test
//    public void test_1() throws Exception {
//        ResultSet rs = connection.createStatement().executeQuery("select * from authors where id_author = 1");
//        System.out.println(rs.getString(2));
//        assertTrue(rs.next());
//        assertEquals(rs.getString("title"), ("Grey T-Shirt"));
//
//
//        IDataSet expectedDataSet = getDataSet();
//    }
//}