package string_intern;

public class Main {
    public static void main(String[] args) {
        String str1 = new String("str");
        String str2 = new String("str");
        System.out.println(str1 == str2);
        System.out.println(str1.equals(str2));
        System.out.println("----------");
        str1.intern();
        str2.intern();
        System.out.println(str1 == str2);
        System.out.println(str1.intern() == str2.intern());
        System.out.println(str1.equals(str2));
        System.out.println("----------");
        String str3 = str2.intern();
        System.out.println(str2 == str3);
        System.out.println(str1 == str3);
        System.out.println(str1 == str2);
        System.out.println(str1.equals(str3));
        System.out.println("----------");

        String s1 = "Javatpoint";
        String s2 = s1.intern();
        String s3 = new String("Javatpoint");
        String s4 = s3.intern();
        System.out.println(s1 == s2); // Правда
        System.out.println(s1 == s3); // Ложь
        System.out.println(s1 == s4); // Правда
        System.out.println(s2 == s3); // Ложь
        System.out.println(s2 == s4); // Правда
        System.out.println(s3 == s4); // Ложь

    }
}
