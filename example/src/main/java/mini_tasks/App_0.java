package mini_tasks;

//1)
public class App_0 {
    public static void main(String[] args) {
        boolean x;
        if (x = true)
            System.out.println("A");
        else
            if (x) System.out.println("B");
        if (!x) System.out.println("C");
        else System.out.println("D");
    }
}

//2)
//public class App_0 {
//    static void m(int x) {
//        System.out.println("int");
//    }
//    static void m(String x) {
//        System.out.println("String");
//    }
//    public static void main(String[] args) {
//        m(null);
//    }
//}

//3)
//public class App_0 {
//    public static void main(String[] args) {
//        for (int i = 0; i < 4; i++)
//            switch (i) {
//                case 0:
//                    System.out.println("a");
//                    break;
//                case 1:
//                    System.out.println("b");
//                case 2:
//                    System.out.println("c");
//                    break;
//                default:
//                    System.out.println("d");
//            }
//    }
//}

//4)
//public class App_0 {
//    public static void main(String[] args) {
//        String s1 = new String("Hello");
//        String s2 = new String("Hello");
//        System.out.println(s1 == s2);
//        System.out.println(s1 != s2);
//        System.out.println(s1.equals(s2));
//    }
//}

//5)
// public class App_0 {
//    public static void main(String[] args) {
//        String s = "Hello";
//        Object ob = s;
//        System.out.println(ob);
//        s = ob;
//        System.out.println(s);
//    }
//}

//6)
//public class App_0 {
//    public static void main(String[] args) {
//        double x = 0, y = 2, z;
//        z = y / x;
//        System.out.println(z);
//    }
//}

//7)
//public class App_0 {
//    public static void main(String[] args) {
//        int x = 13, y = 3;
//        double z = x / y;
//        System.out.println(z);
//    }
//}

//8)
//public class App_0 {
//    public static void main(String[] args) {
//        int x = 1;
//        while (x <= 10) {
//            if (x == 5) continue;
//            System.out.println(x);
//            ++x;
//        }
//    }
//}

//9)
//public class App_0 {
//    public static void main(String[] args) {
//        int b = 6, n, a = 3;
//        n = (--a) * 10 - a * (b++);
//        System.out.println(n);
//        System.out.println(a);
//        System.out.println(b);
//    }
//}

//10)
//public class App_0 {
//    public static void main(String[] args) {
//        int x, z = 0;
//        for (x = 0; x < 5; x++) {
//            ++z;
//            if (x < 3) continue;
//            if (x > 3) break;
//            ++z;
//        }
//        System.out.println(z);
//    }
//}

//11)
//class A {
//    void f(B b) {
//        System.out.println("BB");
//    }
//    void f(A a) {
//        System.out.println("BA");
//    }
//    void show() {
//        System.out.println("A.show()");
//    }
//}
//
//class B extends A {
//    void f(B b) {
//        System.out.println("AB");
//    }
//    void f(A a) {
//        System.out.println("AA");
//    }
//    void show() {
//        System.out.println("B.show()");
//    }
//}
//
//public class App_0 {
//    public static void main(String[] args) {
//        A a = new A();
//        B b = new B();
//        a.f(a);
//        b.f(a);
//        a = new B();
//        a.show();
//    }
//}

//12)
//public class App_0 {
//    static String m(int i) {
//        return "int";
//    }
//    static String m(double i) {
//        return "double";
//    }
//    public static void main(String[] args) {
//        short a1 = 1;
//        float b1 = 2;
//        System.out.println(m(a1) + " : " + m(b1));
//    }
//}

//13)
//public class App_0 {
//    static void m(Object x) {
//        System.out.println("Object");
//    }
//    static void m(String x) {
//        System.out.println("String");
//    }
//    public static void main(String[] args) {
//        Object x=null;
//        m(x);
//        m(null);
//    }
//}

//14)
//class A {
//    void m1(A a) {
//        System.out.println("A");
//    }
//}
//class B extends A {
//    void m1(B b) {
//        System.out.println("B");
//    }
//}
//class C extends B {
//    void m1(C c) {
//        System.out.println("C");
//    }
//}
//public class App_0 {
//    public static void main(String[] args) {
//        A a=new A();
//        B b=new B();
//        C c=new C();
//        c.m1(c);
//        c.m1(b);
//        c.m1(a);
//    }
//}

//Result: 6,5;
//+   : 4(#4,5,7,9);
//-   : 5;
//+-  : 5(#3,6,11,12,13);


