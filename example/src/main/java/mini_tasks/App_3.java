package mini_tasks;

//1)
public class App_3 {
    public static void main(String[] args) {
        Sub sub = new Sub();
        String str = sub.str;
        System.out.println(str);
    }
}

class Sub extends Base {
    public Sub() {
        super("string");
    }
}

class Base {
    String str;

    public Base(String str) {
        this.str = str;
    }
}

//2)
//public class App_3 {
//    public static void main(String[] args) {
//        Z z = new Z();
//        z.m();
//    }
//}
//
//abstract class X {
//    public abstract void m();
//}
//
//interface Y {
//    void m();
//}
//
//class Z extends X implements Y {
//
//    @Override
//    public void m() {
//        System.out.println("fail");
//    }
//}

//3)
//public class App_3 {
//    public static void main(String[] args) {
//        Z.m();
//    }
//}
//
//interface Z {
//    static void m() {
//    }
//}

//4)
//public class App_3 {
//    public static void main(String[] args) {
//        Season s = Season.FALL;
//        System.out.println(s);
//    }
//}
//
//enum Season {
//    WINTER, SPRING, FALL, SUMMER
//}

//5)
//public class App_3 {
//    public static void main(String[] args) {
//        String str1 = "java";
//        String str2 = "java";
//        String str3 = new String("java");
//        System.out.println(str1 == str2 && str1 == str3);
//    }
//}