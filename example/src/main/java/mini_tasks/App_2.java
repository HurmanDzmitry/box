package mini_tasks;

//1)
public class App_2 {
    public static void main(String[] args) {
        String str1 = "strings";
        String str2 = new String("strings");
        String str3 = "strings";
        if (str1 == str2)
            System.out.println("a");
        else System.out.println("b");
        if (str1 == str3)
            System.out.println("c");
        else
            System.out.println("d");
    }
}

//2)
//public class App_2 {
//    public static void main(String[] args) {
//        Item it1 = new Item();
//        Item it2 = new Item();
//        it2.setX(10);
//        System.out.println(it1.equals(it2));
//    }
//}
//
//class Item {
//    private int x;
//
//    public Item() {
//        x = 10;
//    }
//
//    public void setX(int a) {
//        x = a;
//    }
//
//    public int getX() {
//        return x;
//    }
//}

//3)
//public class App_2 {
//    public static void main(String[] args) {
//        Number num2 = null;
//        Object obj = null;
//        Integer int3 = new Integer(67);
//        Double d = new Double(7.7);
//
//        num2 = d;
//        obj = int3;
//
//        int3 = (Integer) obj;
//
//        int3 = (Integer) num2;
//
//        d = (Integer) obj;
//
//        String s = (String) int3;
//    }
//}

//4)
//public class App_2 {
//    public static void main(String[] args) {
//        char ch = 'a';
//        short sh = 345;
//        long lon = 1_000_000_000_000L;
//        float f = 3.45f;
//
//        Overload.f(ch);
//        Overload.f(sh);
//        Overload.f(lon);
//        Overload.f(f);
//    }
//}
//
//class Overload {
//    public static void f(int x) {
//        System.out.println("int");
//    }
//
//    public static void f(float x) {
//        System.out.println("float");
//    }
//
//    public static void f(double x) {
//        System.out.println("double");
//    }
//}

//5)
//public class App_2 {
//    public static void main(String[] args) {
//        StringBuilder s3=new StringBuilder("java");
//        System.out.println(s3);
//        System.out.println(s3.length());
//        System.out.println(s3.capacity());
//        s3.append("12345678901234511");
//        System.out.println(s3);
//        System.out.println(s3.length());
//        System.out.println(s3.capacity());
//    }
//}

//6)
//public class App_2 {
//    public static int x = 777;
//    public static App_2 app_2 = new App_2();
//
//    private App_2() {
//        System.out.println("x= " + x);
//    }
//
//    public static void main(String[] args) {
//        System.out.println(App_2.x);
//    }
//}

//7)
//public class App_2 {
//    public static void main(String[] args) {
//        Integer int1 = new Integer(145);
//        Integer int2 = new Integer(45);
//        Integer int3 = new Integer(145);
//        Integer int4 = 145;
//        Integer int5 = 145;
//        Integer int6 = 45;
//        Integer int7 = 45;
//        int x = 45;
//        System.out.println(int1 > int2);
//        System.out.println(x < int1);
//        System.out.println(x < int3);
//        System.out.println(int1 == int4);
//        System.out.println(int4 == int5);
//        System.out.println(int6 == int7);
//        System.out.println(int2 == x);
//    }
//}

//8)
//public class App_2 {
//    public static void main(String[] args) {
//        byte x = 50;
//        byte y = 70;
//        byte z = 1;
//        byte rez = (byte) (x + y + z);
//        System.out.println(rez);
//    }
//}

//9)
//public class App_2 {
//    public static void main(String[] args) {
//        int a = 2;
//        int b = 5;
//        a = add(b++, a++ + b++);
//        System.out.println(a);
//        System.out.println(b);
//    }
//
//    public static int add(int x, int y) {
//        int z;
//        System.out.println(x);
//        System.out.println(y);
//        z = x + y;
//        return z;
//    }
//}

//10)
//public class App_2 {
//    public static void main(String[] args) {
//        int x = 10;
//        Integer y = 10;
//        Foo z = new Foo(10);
//        meth(x, y, z);
//        System.out.println(x + y + z.x );
//    }
//
//    public static void meth(int x, Integer y, Foo z) {
//        x++;
//        y++;
//        z.x++;
//    }
//}
//
//class Foo {
//    public int x;
//
//    public Foo(int x) {
//        this.x = x;
//    }
//}

//11
//public class App_2 {
//    public static void main(String[] args) {
//        Def d = new Def();
//        d.meth();
//    }
//}
//
//class Def {
//    int a;
//    String name;
//
//    public void meth() {
//        int b;
//        int[] c = new int[5];
//        System.out.println(a);
//        System.out.println(name);
//        System.out.println(b);
//        System.out.println(c);
//    }
//}