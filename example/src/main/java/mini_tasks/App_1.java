package mini_tasks;

//1)
//public class App_1 {
//    public static void main(String[] args) {
//        boolean x;
//        if (x == true)
//            System.out.println("A");
//        else if (x)
//            System.out.println("B");
//        if (!x)
//            System.out.println("C");
//        else System.out.println("D");
//    }
//}

//2)
//public class App_1 {
//    public static void main(String[] args) {
//        double d = 1000000e100;
//        int x = (int) d;
//        int y = (int) -d;
////        System.out.println(d);
////        System.out.println(Double.MAX_VALUE);
//        System.out.println(x);
//        System.out.println(y);
//    }
//}

//3)
//public class App_1 {
//    public static void main(String[] args) {
//        double x = 0;
//        double y = 2;
//        double z;
//        z = y / x;
//        System.out.println(z);
//    }
//}

//4)
//public class App_1 {
//    public static void main(String[] args) {
//        int x = 0;
//        int y = 2;
//        double z = y / x; /* for drop exc can (double) y / x */
//        System.out.println(z);
//    }
//}

//5)
public class App_1 {
    public static void main(String[] args) {
        int x = 325;
        byte y = (byte) x;
        System.out.println(x);
        System.out.println(y);
    }
}

//6)
//public class App_1 {
//    private static int j = 0;
//
//    private static boolean methodB(int k) {
//        j += k;
//        return true;
//    }
//
//    private static void methodA(int i) {
//        boolean b;
//        b = i < 10 | methodB(4);
//        b = i < 10 || methodB(8);
//    }
//
//    public static void main(String[] args) {
//        methodA(0);
//        System.out.println(j);
//    }
//}

//7)
//final class Complex {
//    private double re, im;
//
//    public Complex(double re, double im) {
//        System.out.println("Complex(constructor(re,mi))");
//        this.re = re;
//        this.im = im;
//    }
//
//    Complex(Complex c) {
//        this(c.im, c.re);
//        System.out.println("Complex(constructor(c))");
//    }
//
//    public String toString() {
//        return "(" + re + " + " + im + "i)";
//    }
//}
//
//public class App_1 {
//    public static void main(String[] args) {
//        Complex c1 = new Complex(10, 15);
//        Complex c2 = new Complex(c1);
//        Complex c3 = c1;
//        System.out.println(c2);
//        System.out.println(c1 == c2);
//        System.out.println(c2 == c3);
//        System.out.println(c1 == c3);
//        System.out.println(c1.equals(c2));
//        System.out.println(c2.equals(c3));
//        System.out.println(c1.equals(c3));
//    }
//}

//8)
//public class App_1 {
//    public static void main(String[] args) {
//        A a1 = new A();
//        A a2 = new A();
//    }
//}
//
//class A {
//    static int a = 0;
//    int b = a++;
//
//    {
//        a = a + 2;
//    }
//
//    static int c = a;
//
//    static {
//        a = a + 5;
//    }
//
//    int d = a;
//
//    public A() {
//        System.out.println(a);
//        System.out.println(b);
//        System.out.println(c);
//        System.out.println(d);
//    }
//
//    static {
//        a = a + 9;
//    }
//}