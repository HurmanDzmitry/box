//package task_ObjectStyle;
//
//import java.text.ParseException;
//import java.text.SimpleDateFormat;
//import java.util.*;
//
//public class FreeTime {
//    public static String MostFreeTime(String[] strArr) {
//        List<Long> list = new ArrayList<>();
//        SimpleDateFormat format = new SimpleDateFormat("hh:mm" + "a", Locale.US);
//        for (String s : strArr) {
//            int colon = s.indexOf('-');
//            s = s.replaceAll(" ", "");
//            String start = s.substring(0, colon);
//            String finish = s.substring(colon + 1);
//            try {
//                Date dateStart = format.parse(start);
//                Date dateFinish = format.parse(finish);
//                list.add(dateStart.getTime());
//                list.add(dateFinish.getTime());
//            } catch (ParseException e) {
//                e.printStackTrace();
//            }
//        }
//        Collections.sort(list);
//        long max = 0;
//        for (int i = 1; i < list.size() - 1; i += 2) {
//            long x = list.get(i + 1) - list.get(i);
//            if (x > max)
//                max = x;
//        }
//        Date freTime = new Date(max - 10800000);
//        format.applyPattern("HH:mm");
//        strArr[0] = format.format(freTime);
//
//// code goes here
//        return strArr[0];
//    }
//
//    public static void main(String[] args) {
//        String[] array = {"12:15PM-02:00PM","09:00AM-10:00AM","10:30AM-12:00PM"};
//        System.out.print(MostFreeTime(array));
//    }
//}