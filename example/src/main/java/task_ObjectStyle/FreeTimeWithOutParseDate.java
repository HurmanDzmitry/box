package task_ObjectStyle;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class FreeTimeWithOutParseDate {
    public static String MostFreeTime(String[] strArr) {
        List<String> list = new ArrayList<>();
        for (String s : strArr) {
            int colon = s.indexOf('-');
            s = s.replaceAll(" ", "");
            String start = s.substring(0, colon);
            String finish = s.substring(colon + 1);
            list.add(start);
            list.add(finish);
        }
        List<Integer> integerList = new ArrayList<>();
        for (String s : list) {
            if (s.substring(5).equals("PM") && !s.substring(0, 2).equals("12")) {
                s = 12 + Integer.valueOf(s.substring(0, 2)) + s.substring(2);
            }
            integerList.add(Integer.valueOf(s.substring(0, 2)) * 60 + Integer.valueOf(s.substring(3, 5)));
        }
        Collections.sort(integerList);
        int max = 0;
        for (int i = 1; i < integerList.size() - 1; i += 2) {
            int x = integerList.get(i + 1) - integerList.get(i);
            if (x > max)
                max = x;
        }
        int hour = max / 60;
        int minute = max - hour * 60;
        String first;
        String last;
        if (hour >= 10)
            first = String.valueOf(hour);
        else {
            if (hour == 0)
                first = "00";
            else first = "0" + hour;
        }
        if (minute >= 10)
            last = String.valueOf(minute);
        else {
            if (minute == 0)
                last = "00";
            else last = "0" + minute;
        }
        strArr[0] = first + ':' + last;
        return strArr[0];
    }

    public static void main(String[] args) {
        String[] array = {"01:52AM-01:55AM", "01:00AM-01:10AM", "01:20AM-01:50AM"};
        System.out.print(MostFreeTime(array));
    }
}