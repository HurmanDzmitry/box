package task_ObjectStyle;

import java.util.Map;
import java.util.TreeMap;

public class DoubleKey {
    public static String GroupTotals(String[] strArr) {
        Map<String, Integer> map = new TreeMap<>();
        for (String s : strArr) {
            int colon = s.indexOf(':');
            String key = s.substring(0, colon);
            String value = s.substring(colon + 1);
            Integer valueInt = Integer.parseInt(value);
//            if (map.get(key) != null)
            if (map.containsKey(key))
                valueInt += map.get(key);
            map.put(key, valueInt);
        }

//            for (String str : map.keySet()) {
//                if (key.equals(str)) {
//                    valueInt += map.get(str);
//                }
//            }
//            map.put(key, valueInt);
//        }
        StringBuilder out = new StringBuilder(" ");
        for (String str : map.keySet()) {
            if (map.get(str) != 0) {
                out.append(str).append(":").append(map.get(str)).append(',');
            }
        }
        strArr[0] = out.substring(1, out.length() - 1);
        return strArr[0];
    }

    public static void main(String[] args) {
        String[] array = {"A:12", "E:-1", "A:12"};
        System.out.println(GroupTotals(array));
    }
}
