package task_System_Technologies;

public class TestApplication1 {

    public static void main(String[] args) {
        Parent parent = new Parent();
        parent.print();
        Parent child = new Child();
        child.print();
    }

    static class Parent {
        protected String fio;

        public Parent() {
            fio = "анна мария оглы";
        }

        private void print() {
            String str = "";
            if (getClass().getSimpleName().equals("Parent")) {
                str = fio + '!';
            }
            if (getClass().getSimpleName().equals("Child")) {
                fio = fio.toLowerCase();
                char[] chars = fio.toCharArray();
                chars[0] = Character.toUpperCase(chars[0]);
                for (int i = 1; i < chars.length; i++) {
                    if (chars[i - 1] == ' ' ||
                            chars[i - 1] == '\'' ||
                            chars[i - 1] == '-') {
                        chars[i] = Character.toUpperCase(chars[i]);
                    }
                }
                str = String.valueOf(chars);
            }

            System.out.println(str);
        }
    }

    static class Child extends Parent {
        public Child() {
            fio = "АН'НА-МАРИЯ оглы";
        }
    }
}
