package acces.one;

public class A {
    public int a = 1;
    protected int b1 = 2;
    static protected int b2 = 2;
    int c = 3;
    private int d = 4;

    public static void main(String[] args) {
        A a = new A();
        System.out.println(a.a);
        System.out.println(a.b1);
        System.out.println(b2);
        System.out.println(a.c);
        System.out.println(a.d);
    }
}
