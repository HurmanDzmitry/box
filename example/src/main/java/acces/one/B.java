package acces.one;

public class B /*extends A*/ /*no matter*/ {
    public static void main(String[] args) {
        A a = new A();
//      public
        System.out.println(a.a);
//      protected
        System.out.println(a.b1);
        System.out.println(A.b2);
//      default
        System.out.println(a.c);
    }
}
