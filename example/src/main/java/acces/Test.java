package acces;

import acces.one.A;

public class Test extends A {
    public static void main(String[] args) {
        A a = new A();
//      only public
        System.out.println(a.a);
//      for non-static protected
//      System.out.println(a.b1); fail

//      for static protected
        System.out.println(A.b2);

        Test test = new Test();
//      public
        System.out.println(test.a);
//      protected
        System.out.println(test.b1);

    }
}
