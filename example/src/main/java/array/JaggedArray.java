package array;

public class JaggedArray {
    public static void main(String[] args) {

//        int[][] array = {{1, 2},{5, 4, 6}};

        int[][] array = new int[2][];
        array[0] = new int[]{1, 2};
        array[1] = new int[]{5, 4, 6};
        show(array);
    }

    private static void show(int[][] array) {
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++)
                System.out.print(array[i][j] + " ");
            System.out.println();
        }
    }
}