package task_GP_Solution.finish;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.Scanner;

public class Hurman_Task_1 {
    public static void main(String[] args) throws FileNotFoundException {
//        Scanner sc = new Scanner(new File("input.txt"));
//        int n = sc.nextInt();
//        List<Long> list = new ArrayList<>(n);
//        for (int i = 0; i < n; i++) {
//            list.add(sc.nextLong());
//        }
//        Collections.sort(list);
//        PrintWriter pw = new PrintWriter(new File("output.txt"));
//        pw.print(list.get(0) + " ");
//        pw.print(list.get(n - 1) + " ");
//        pw.print(list.get(1) + " ");
//        pw.print(list.get(n - 2));
//        pw.close();

        Scanner sc = new Scanner(new File("input.txt"));
        int n = sc.nextInt();
        long[] array = new long[n];
        for (int i = 0; i < n; i++) {
            array[i] = sc.nextInt();
        }
        Arrays.sort(array);
        PrintWriter pw = new PrintWriter(new File("output.txt"));
        pw.print(array[0] + " ");
        pw.print(array[n - 1] + " ");
        pw.print(array[1] + " ");
        pw.print(array[n - 2]);
        pw.close();

//        Scanner sc = new Scanner(new File("input.txt"));
//        int n = sc.nextInt();
//        TreeSet<Long> set = new TreeSet<>();
//        for (int i = 0; i < n; i++) {
//            set.add(sc.nextLong());
//        }
//        PrintWriter pw = new PrintWriter(new File("output.txt"));
//        pw.print(set.first() + " ");
//        set.remove(set.first());
//        pw.print(set.last() + " ");
//        set.remove(set.last());
//        pw.print(set.first() + " ");
//        pw.print(set.last() + " ");
//        pw.close();

//        Scanner sc = new Scanner(new File("input.txt"));
//        int n = sc.nextInt();
//        long[] array = new long[n];
//        for (int i = 0; i < n; i++) {
//            array[i] = sc.nextInt();
//        }
//        long max1 = Math.max(array[0], array[1]);
//        long max2 = Math.min(array[0], array[1]);
//        long min1 = max2;
//        long min2 = max1;
//        for (int i = 2; i < array.length; i++) {
//            if (array[i] > max1) {
//                max2 = max1;
//                max1 = array[i];
//            } else {
//                if (array[i] > max2) {
//                    max2 = array[i];
//                }
//            }
//            if (array[i] < min1) {
//                min2 = min1;
//                min1 = array[i];
//            } else {
//                if (array[i] < min2) {
//                    min2 = array[i];
//                }
//            }
//        }
//        PrintWriter pw = new PrintWriter(new File("output.txt"));
//        pw.print(min1 + " ");
//        pw.print(max1 + " ");
//        pw.print(min2 + " ");
//        pw.print(max2);
//        pw.close();
    }
}