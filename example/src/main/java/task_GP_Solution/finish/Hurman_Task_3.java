package task_GP_Solution.finish;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Hurman_Task_3 {
    private static PrintWriter pw;

    public static void main(String[] args) throws FileNotFoundException {
        Scanner sc = new Scanner(new File("input.txt"));
        pw = new PrintWriter(new File("output.txt"));
        String begin = sc.nextLine();
        char[] array = begin.replaceAll("\\s", "").toCharArray();
        List<Character> list = new ArrayList<>(array.length);
        for (Character c : array) {
            list.add(c);
        }
        label:
        while (list.size() > 1) {
            for (int i = 0; i < list.size(); i++) {
                if (list.get(i).equals('(')
                        || list.get(i).equals(')')) {
                    System.out.println("---");
                } else if ((list.get(i).equals('*')
                        || list.get(i).equals('/')
                        || list.get(i).equals('%')
                        || list.get(i).equals('!'))
                        && (i != 0 || i != list.size() - 1)) {
                    System.out.println("catch");
                    char c = (char) second(list.get(i), list, i);
                    if (!list.get(i).equals('!')) {
                        list.remove(i);
                    }
                    list.remove(i);
                    list.set(i - 1, c);
                } else if (list.get(i).equals('-')
                        || list.get(i).equals('+')
                        && (i != 0 || i != list.size() - 1)) {
                    char c = (char) third(list.get(i), list, i);
                    list.remove(i);
                    list.remove(i);
                    list.set(i - 1, c);
                } else {
                    pw.print("Input is not correct");
                    list.clear();
                    break label;
                }
            }
        }
        if (!list.isEmpty())
            pw.print(list.get(0));
        pw.close();
    }

    private static double second(Character s, List<Character> list, int index) {
        double d = 0;
        switch (s) {
            case '*':
                d = (Double.valueOf(list.get(index - 1)) * Double.valueOf(list.get(index + 1)));
                break;
            case '/':
                d = (Double.valueOf(list.get(index - 1)) / Double.valueOf(list.get(index + 1)));
                break;
            case '%':
                d = (Double.valueOf(list.get(index - 1)) % Double.valueOf(list.get(index + 1)));
                break;
            default:
                if (Double.valueOf(list.get(index - 1)) % 1 == 0) {
                    d = fact(Double.valueOf(list.get(index - 1)));
                } else try {
                    throw new Exception();
                } catch (Exception e) {
                    pw.print("Input is not correct");
                }
        }
        return d;
    }

    private static double third(Character s, List<Character> list, int index) {
        double d;
        if ('+' == s) {
            d = (Double.valueOf(list.get(index - 1)) + Double.valueOf(list.get(index + 1)));
        } else {
            d = (Double.valueOf(list.get(index - 1)) - Double.valueOf(list.get(index + 1)));
        }
        return d;
    }

    private static double fact(double i) {
        if (i == 0)
            return 1;
        return i * fact(i - 1);
    }
}
