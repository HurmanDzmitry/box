package task_GP_Solution.finish;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Scanner;

public class Hurman_Task_2 {
    public static void main(String[] args) throws FileNotFoundException {
        Scanner sc = new Scanner(new File("input.txt"));
        int n = sc.nextInt();
        List<Integer> list = new ArrayList<>(n);
        for (int i = 0; i < n; i++) {
            list.add(sc.nextInt());
        }
        list.sort(new NumberComparator());
        PrintWriter pw = new PrintWriter(new File("output.txt"));
        for (Integer i : list) {
            pw.print(i + " ");
        }
        pw.close();
    }

    static class NumberComparator implements Comparator<Integer> {
        @Override
        public int compare(Integer o1, Integer o2) {
            if (o1 >= o2 * 10)
                return 1;
            if (o1 <= o2 * 0.1)
                return -1;
            else return 0;
        }
    }
}
