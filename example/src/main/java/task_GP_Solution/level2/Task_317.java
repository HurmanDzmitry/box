package task_GP_Solution.level2;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Scanner;

public class Task_317 {
    public static void main(String[] args) throws FileNotFoundException {
        Scanner sc = new Scanner(new File("input.txt"));
        int x = sc.nextInt();
        int y = sc.nextInt();
        int z = sc.nextInt();
        int w = sc.nextInt();
        int counter = 0;
        for (int i = 0; i <= w / x; i++) {
            for (int j = 0; j <= (w - i * x) / y; j++) {
                if ((w - i * x - j * y) % z == 0)
                    counter++;
            }
        }
        PrintWriter pw = new PrintWriter(new File("output.txt"));
        pw.print(counter);
        pw.close();
    }
}
