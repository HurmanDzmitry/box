package task_GP_Solution.level2;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Scanner;

public class Task_53 {
    public static void main(String[] args) throws FileNotFoundException {
        Scanner sc = new Scanner(new File("input.txt"));
        int n = sc.nextInt();
        int m = sc.nextInt();
        int blue = 0;
        int green = 0;
        int red = 0;
        int black = 0;
        int value;
        for (int i = 1; i <= n; i++) {
            for (int j = 1; j <= m; j++) {
                value = i * j;
                if (value % 5 == 0)
                    blue++;
                else if (value % 3 == 0)
                    green++;
                else if (value % 2 == 0)
                    red++;
                else black++;
            }
        }
        PrintWriter pw = new PrintWriter(new File("output.txt"));
        pw.println("RED : " + red);
        pw.println("GREEN : " + green);
        pw.println("BLUE : " + blue);
        pw.println("BLACK : " + black);
        pw.close();
    }
}
