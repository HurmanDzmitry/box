package task_GP_Solution.level2;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Scanner;

public class Task_654 {
    public static void main(String[] args) throws FileNotFoundException {
        Scanner sc = new Scanner(new File("input.txt"));
        int n = sc.nextInt();
        long sum = 0;
        int previos = sc.nextInt();
        int max = previos;
        for (int i = 1; i < n; i++) {
            int current;
            current = sc.nextInt();
            max = Math.max(current, max);
            if (current > previos) {
                sum += current - previos;
            }
            previos = current;
        }
        sum += max - previos;
        PrintWriter pw = new PrintWriter(new File("output.txt"));
        pw.print(sum);
        pw.close();
    }
}
