package task_GP_Solution.level2;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Scanner;

public class Task_46 {
    public static void main(String[] args) throws FileNotFoundException {
        Scanner sc = new Scanner(new File("input.txt"));
        int numberAfterDot = sc.nextInt();
        String number = "2.7182818284590452353602875";
        number = substring(number, numberAfterDot);
        PrintWriter pw = new PrintWriter(new File("output.txt"));
        pw.print(number);
        pw.close();
    }

    private static String substring(String number, int numberAfterDot) {
        if (numberAfterDot == 0)
            number = number.substring(0, 1).replace('2', '3');
        else {
            number = rounding(number, numberAfterDot);
        }
        return number;
    }

    private static String rounding(String number, int numberAfterDot) {
        int end = Integer.parseInt(number.substring(numberAfterDot + 1, numberAfterDot + 2));
        int afterEnd;
        if (number.length() > numberAfterDot + 2) {
            afterEnd = Integer.parseInt(number.substring(numberAfterDot + 2, numberAfterDot + 3));
            if (afterEnd >= 5)
                end++;
        }
        return number.substring(0, numberAfterDot + 1) + end;
    }
}