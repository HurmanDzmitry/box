package task_GP_Solution.level1;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Scanner;

public class Task_550 {
    public static void main(String[] args) throws FileNotFoundException {
        Scanner sc = new Scanner(new File("input.txt"));
        int year = sc.nextInt();
        PrintWriter pw = new PrintWriter(new File("output.txt"));
        pw.print(date(year));
        pw.close();
    }

    private static String date(int year) {
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        GregorianCalendar calendar = new GregorianCalendar();
        calendar.setGregorianChange(new Date(Long.MIN_VALUE));
        calendar.set(year, Calendar.JANUARY, 256);
        sdf.setCalendar(calendar);
        return sdf.format(calendar.getTime());
    }
}