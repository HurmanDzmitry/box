package task_GP_Solution.level1;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Scanner;

public class Task_195 {
    public static void main(String[] args) throws FileNotFoundException {
        Scanner sc = new Scanner(new File("input.txt"));
        int panels = sc.nextInt();
        int length = sc.nextInt();
        int width = sc.nextInt();
        PrintWriter pw = new PrintWriter(new File("output.txt"));
        pw.print(panels * length * width * 2);
        pw.close();
    }
}
