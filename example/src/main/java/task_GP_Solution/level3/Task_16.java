package task_GP_Solution.level3;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Scanner;

public class Task_16 {
    public static void main(String[] args) throws FileNotFoundException {
        Scanner sc = new Scanner(new File("input.txt"));
        int n = sc.nextInt();
        int count = 0;

        for (int level = 1; level <= n; ++level) {
            count += getCount(level, n - level);
        }

        PrintWriter pw = new PrintWriter(new File("output.txt"));
        pw.print(count);
        pw.close();
    }

    private static int getCount(int prevLevel, int n) {
        if (0 == n)
            return 1;
        int count = 0;
        for (int level = 1; level < prevLevel; ++level) {
            if ((n - level) < 0)
                break;
            count += getCount(level, n - level);
        }
        return count;
    }
}
