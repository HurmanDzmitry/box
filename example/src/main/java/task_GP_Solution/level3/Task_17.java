package task_GP_Solution.level3;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Task_17 {
    public static void main(String[] args) throws FileNotFoundException {
        Scanner sc = new Scanner(new File("input.txt"));
        int n = sc.nextInt();
        List<Integer> values = new ArrayList<>(n - 1);
        for (int i = 0; i < n - 1; i++) {
            values.add(sc.nextInt());
        }
        int firstSector = values.get(0);
        int counter = 1;
        int sizeArray = values.size();
        for (int i = 1; i < sizeArray; i++) {
            if (sizeArray % i == 0) {
                int indexRepeat = values.subList(i, sizeArray).indexOf(firstSector) + i;
                if (indexRepeat != -1) {
                    List<Integer> pattern = values.subList(0, i);
                    for (int j = i; j < sizeArray; j += indexRepeat) {
                        if (values.subList(j, j + i).equals(pattern)) {
                            counter++;
                        } else break;
                    }
                    if (counter == sizeArray / i) {
                        break;
                    } else counter = 1;
                }
            }
        }
        PrintWriter pw = new PrintWriter(new File("output.txt"));
        pw.print(sizeArray / counter);
        pw.close();
    }
}
