package task_GP_Solution.level3;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.math.BigInteger;
import java.util.Scanner;

public class Task_7 {
    public static void main(String[] args) throws FileNotFoundException {
        Scanner sc = new Scanner(new File("input.txt"));
        BigInteger one = sc.nextBigInteger();
        BigInteger two = sc.nextBigInteger();
        BigInteger three = sc.nextBigInteger();
        BigInteger max = one.max(two.max(three));
        PrintWriter pw = new PrintWriter(new File("output.txt"));
        pw.print(max);
        pw.close();
    }
}
