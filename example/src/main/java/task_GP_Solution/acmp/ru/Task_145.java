package task_GP_Solution.acmp.ru;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.math.BigInteger;
import java.util.Scanner;

public class Task_145 {
    public static void main(String[] args) throws FileNotFoundException {
        Scanner sc = new Scanner(new File("input.txt"));
        BigInteger one = sc.nextBigInteger();
        BigInteger two = sc.nextBigInteger();
        PrintWriter pw = new PrintWriter(new File("output.txt"));
        pw.print(one.divide(two));
        pw.close();
    }
}
