package task_GP_Solution.acmp.ru;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Scanner;

public class Task_62 {
    public static void main(String[] args) throws FileNotFoundException {
        Scanner sc = new Scanner(new File("input.txt"));
        PrintWriter pw = new PrintWriter(new File("output.txt"));
        String value = sc.next();
        char letter = value.charAt(0);
        char number = value.charAt(1);
        if (((letter == 'A' || letter == 'C' || letter == 'E' || letter == 'G') && number % 2 == 1)
                || ((letter == 'B' || letter == 'D' || letter == 'F' || letter == 'H') && number % 2 == 0)) {
            pw.print("BLACK");
        } else pw.print("WHITE");
        pw.close();
    }
}
