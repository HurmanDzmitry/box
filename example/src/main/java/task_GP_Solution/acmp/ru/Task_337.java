package task_GP_Solution.acmp.ru;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Scanner;

public class Task_337 {
    private static int light;

    public static void main(String[] args) throws FileNotFoundException {
        Scanner sc = new Scanner(new File("input.txt"));
        int bulbs = sc.nextInt();
        int inversions = sc.nextInt();
        byte[] array = new byte[bulbs];
        for (int i = 0; i < bulbs; i++) {
            array[i] = 0;
        }
        int index;
        int grow;
        int x;
        for (int i = 0; i < inversions; i++) {
            index = sc.nextInt();
            grow = index;
            while (grow <= bulbs) {
                x = array[grow - 1];
                array[grow - 1] = (byte) inversion(x);
                grow += index;
            }
        }
        PrintWriter pw = new PrintWriter(new File("output.txt"));
        pw.print(light);
        pw.close();
    }

    private static int inversion(int x) {
        if (x == 0) {
            light++;
            return 1;
        } else {
            light--;
            return 0;
        }
    }
}
