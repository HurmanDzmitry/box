package task_GP_Solution.acmp.ru;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Map;
import java.util.Scanner;
import java.util.TreeMap;

public class Task_561 {
    public static void main(String[] args) throws FileNotFoundException {
        Scanner sc = new Scanner(new File("input.txt"));
        int[][] array = new int[sc.nextInt()][];
        for (int i = 0; i < array.length; i++) {
            array[i] = new int[sc.nextInt() + 1];
            for (int j = 0; j < array[i].length; j++) {
                array[i][j] = sc.nextInt();
            }
        }
        Map<Long, Integer> map = new TreeMap<>();
        for (int i = 0; i < array.length; i++) {
            map.put(degree(array[i]), i + 1);
        }
        output(map);
    }

    //    private static long degree(int[] array) {
//        long result = 1;
//        for (int i = array.length - 1; i >= 0; i--) {
//            result = (long) Math.pow(array[i], result);
//        }
//        return result;
//    }

    private static long degree(int[] array) {
        long result = 1;
        for (int i = array.length - 1; i >= 0; i--) {
            result *=array[i] ;
        }
        return result;
    }

    private static void output(Map<Long, Integer> map) throws FileNotFoundException {
        StringBuilder sb = new StringBuilder();
        PrintWriter pw = new PrintWriter(new File("output.txt"));
        for (Long i : map.keySet()) {
            sb.append(map.get(i)).append(' ');
        }
        pw.print(sb.toString().trim());
        pw.close();
    }
}
