package task_GP_Solution.acmp.ru;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Scanner;

public class Task_486_unsolved {
    public static void main(String[] args) throws FileNotFoundException {
        Scanner sc = new Scanner(new File("input.txt"));
        long fishman = sc.nextInt();
        long remain = sc.nextInt();

        long i;
        for (i = 1; ; i++) {
            long fish = i;
            int j = 0;
            if (fish % fishman == remain) {
                for (j = 1; j < fishman; j++) {
                    long tmp = fish - fish / fishman - remain;
                    if (tmp % fishman == remain && fish != tmp) {
                        fish = tmp;
                    } else break;
                }
            }
            if (j == fishman) {
                break;
            }
        }

        PrintWriter pw = new PrintWriter(new File("output.txt"));
//        pw.print((long) Math.pow(fishman, fishman) - remain * (fishman - 1));
        pw.print(i);
        pw.close();
    }
}
