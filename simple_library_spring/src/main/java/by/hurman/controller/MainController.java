package by.hurman.controller;

import by.hurman.dao.entity_dao.BookDao;
import by.hurman.entity.Book;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;

@Controller
public class MainController {

    private final BookDao bookDao;

    @Autowired
    public MainController(@Qualifier("bookDaoImpl") BookDao bookDao) {
        this.bookDao = bookDao;
    }

    @GetMapping("/main")
    public String mainPage(Model model) {
        List<Book> books = bookDao.findAll();
        model.addAttribute("books", books);
        return "main";
    }
}
