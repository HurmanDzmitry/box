package by.hurman.controller.util;

public class Uri {

    private static final String CONTEXT = "/library";

    public static final String MAIN = CONTEXT + "/main";
    public static final String SIGN_UP = CONTEXT + "/sign_up";
    public static final String SIGN_UP_BLANK = CONTEXT + "/sign_up_blank";
    public static final String LOG_IN = CONTEXT + "/log_in";
    public static final String LOG_IN_BLANK = CONTEXT + "/log_in_blank";
    public static final String LOG_OUT = CONTEXT + "/log_out";
    public static final String ACCOUNT = CONTEXT + "/account";
    public static final String BOOK = CONTEXT + "/book";
    public static final String BOOKS = CONTEXT + "/books";
    public static final String ADD_BOOK_BLANK = CONTEXT + "/add_book_blank";
    public static final String ADD_BOOK = CONTEXT + "/add_book";
    public static final String AUTHOR = CONTEXT + "/author";

    private Uri() {
    }
}
