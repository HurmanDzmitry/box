package by.hurman.controller.util;

public class JspPath {

    private static final String ROOT = "WEB-INF/jsp";

    public static final String ADD_AUTHOR = ROOT + "/author/add_author.jsp";
    public static final String AUTHOR = ROOT + "/author/author.jsp";
    public static final String AUTHORS = ROOT + "/author/authors.jsp";

    public static final String ADD_BOOK = ROOT + "/book/add_book.jsp";
    public static final String BOOK = ROOT + "/book/book.jsp";
    public static final String BOOKS = ROOT + "/book/books.jsp";

    public static final String ACCOUNT = ROOT + "/user/account.jsp";
    public static final String LOG_IN = ROOT + "/user/log_in.jsp";
    public static final String SIGN_UP = ROOT + "/user/sign_up.jsp";

    public static final String ABOUT_US = ROOT + "/about_us.jsp";
    public static final String MAIN = ROOT + "/main.jsp";
    public static final String SEARCH = ROOT + "/search.jsp";

    private JspPath() {
    }

}
