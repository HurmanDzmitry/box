package by.hurman.controller;

import by.hurman.dao.entity_dao.UserDao;
import by.hurman.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseStatus;

import javax.servlet.http.HttpServletRequest;

@Controller
public class UserController {

    @Autowired
    @Qualifier("userDaoImpl")
    UserDao userDao;

    @Autowired
    MainController mainController;

    @GetMapping("/log_in")
    @ResponseStatus(HttpStatus.OK)
    public String logInBlank() {
        return "user/log_in";
    }

    @PostMapping("/log_in")
    @ResponseStatus(HttpStatus.CREATED)
    public String addUser(HttpServletRequest request, Model model) {
        String login = request.getParameter("login");
        String pass = request.getParameter("password");
        User user = userDao.findByLogin(login);
        if (user != null && pass.equals(user.getPassword())) {
            mainController.mainPage(model);
        } else {
            model.addAttribute("message", true);
        }
        return "user/log_in";
    }
}
