package by.hurman.dao.entity_dao;

import by.hurman.dao.GenericDao;
import by.hurman.entity.Book;

public interface BookDao extends GenericDao<Book> {
}
