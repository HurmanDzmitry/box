package by.hurman.dao.entity_dao;

import by.hurman.dao.GenericDao;
import by.hurman.entity.Author;

public interface AuthorDao extends GenericDao<Author> {
}
