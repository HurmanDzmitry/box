package by.hurman.dao.entity_dao;

import by.hurman.dao.GenericDao;
import by.hurman.entity.User;

public interface UserDao extends GenericDao<User> {

    User findByLogin(String login);
}
