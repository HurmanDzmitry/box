package by.hurman.dao;

import java.util.List;

public interface GenericDao<T> {

    List<T> findAll();

    T findById(int id);

    void delete(int id);

    T save(T entity);

    T update(T entity);
}
