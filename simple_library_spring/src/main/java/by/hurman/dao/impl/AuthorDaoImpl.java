package by.hurman.dao.impl;

import by.hurman.dao.entity_dao.AuthorDao;
import by.hurman.entity.Author;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Qualifier("hibernateAuthorDaoImpl")
public class AuthorDaoImpl implements AuthorDao {

    @Autowired
    @Qualifier("sessionFactory")
    private SessionFactory sessionFactory;

    @Override
    public List<Author> findAll() {
        Session session = sessionFactory.openSession();
        return session.createQuery("select tu from Author tu", Author.class).getResultList();
    }

    @Override
    public Author findById(int id) {
        Session session = sessionFactory.openSession();
        return session.find(Author.class, id);
    }

    @Override
    public void delete(int id) {
        Session session = sessionFactory.openSession();
        session.remove(findById(id));
    }

    @Override
    public Author save(Author entity) {
        Session session = sessionFactory.openSession();
        Transaction transaction = session.getTransaction();
        transaction.begin();
        int newAuthorID = (int) session.save(entity);
        transaction.commit();
        return session.find(Author.class, newAuthorID);

    }

    @Override
    public Author update(Author entity) {
        Session session = sessionFactory.openSession();
        Transaction transaction = session.getTransaction();
        transaction.begin();
        session.saveOrUpdate(entity);
        transaction.commit();
        return session.find(Author.class, entity.getId());
    }
}
