package by.hurman.dao.impl;

import by.hurman.dao.entity_dao.UserDao;
import by.hurman.entity.User;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import javax.persistence.TypedQuery;
import java.util.List;

@Repository
@Qualifier("hibernateUserDaoImpl")
public class UserDaoImpl implements UserDao {

    @Autowired
    @Qualifier("sessionFactory")
    private SessionFactory sessionFactory;

    @Override
    public List<User> findAll() {
        Session session = sessionFactory.openSession();
        return session.createQuery("select tu from User tu", User.class).getResultList();
    }

    @Override
    public User findById(int id) {
        Session session = sessionFactory.openSession();
        return session.find(User.class, id);
    }

    @Override
    public void delete(int id) {
        Session session = sessionFactory.openSession();
        session.remove(findById(id));
    }

    @Override
    public User save(User entity) {
        Session session = sessionFactory.openSession();
        Transaction transaction = session.getTransaction();
        transaction.begin();
        int newUserID = (int) session.save(entity);
        transaction.commit();
        return session.find(User.class, newUserID);

    }

    @Override
    public User update(User entity) {
        Session session = sessionFactory.openSession();
        Transaction transaction = session.getTransaction();
        transaction.begin();
        session.saveOrUpdate(entity);
        transaction.commit();
        return session.find(User.class, entity.getId());
    }

    @Override
    public User findByLogin(String login) {
        Session session = sessionFactory.openSession();
        TypedQuery<User> query = session.createQuery("" +
                "select tu from User tu where tu.login = :login", User.class);
        query.setParameter("login", login);
        return query.getSingleResult();
    }
}
