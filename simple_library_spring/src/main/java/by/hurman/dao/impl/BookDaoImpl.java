package by.hurman.dao.impl;

import by.hurman.dao.entity_dao.BookDao;
import by.hurman.entity.Book;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Qualifier("hibernateBookDaoImpl")
public class BookDaoImpl implements BookDao {

    @Autowired
    @Qualifier("sessionFactory")
    private SessionFactory sessionFactory;

    @Override
    public List<Book> findAll() {
        Session session = sessionFactory.openSession();
        return session.createQuery("select tu from Book tu", Book.class).getResultList();
    }

    @Override
    public Book findById(int id) {
        Session session = sessionFactory.openSession();
        return session.find(Book.class, id);
    }

    @Override
    public void delete(int id) {
        Session session = sessionFactory.openSession();
        session.remove(findById(id));
    }

    @Override
    public Book save(Book entity) {
        Session session = sessionFactory.openSession();
        Transaction transaction = session.getTransaction();
        transaction.begin();
        int newBookID = (int) session.save(entity);
        transaction.commit();
        return session.find(Book.class, newBookID);

    }

    @Override
    public Book update(Book entity) {
        Session session = sessionFactory.openSession();
        Transaction transaction = session.getTransaction();
        transaction.begin();
        session.saveOrUpdate(entity);
        transaction.commit();
        return session.find(Book.class, entity.getId());
    }
}
