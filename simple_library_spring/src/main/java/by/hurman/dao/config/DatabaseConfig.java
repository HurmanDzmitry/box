package by.hurman.dao.config;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@ComponentScan(value = "by.hurman")
@PropertySource("classpath:db.properties")
public class DatabaseConfig {

    @Value("${driverName}")
    private String driverClassName;

    @Value("${url}")
    private String url;

    @Value("${login}")
    private String username;

    @Value("${password}")
    private String password;

    @Value("${maxPoolSize}")
    private int maxPoolSize;

    @Bean(value = "dataSource", destroyMethod = "close")
    public HikariDataSource getDatasource() {
        HikariConfig config = new HikariConfig();
        HikariDataSource ds;
        config.setDriverClassName(driverClassName);
        config.setJdbcUrl(url);
        config.setUsername(username);
        config.setPassword(password);
        config.setMaximumPoolSize(maxPoolSize);
        ds = new HikariDataSource(config);
        return ds;
    }
}
