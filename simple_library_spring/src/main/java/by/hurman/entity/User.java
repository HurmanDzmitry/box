package by.hurman.entity;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.PastOrPresent;
import javax.validation.constraints.Pattern;
import java.io.Serializable;
import java.time.LocalDate;

@Entity
@Table(name = "users")
@Data
@NoArgsConstructor
public class User implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Basic
    @Column(name = "name", length = 30, nullable = false)
    @Pattern(regexp = "^([a-zA-Z-]|[а-яА-Я-]){2,20}$", message = "")
    private String name;

    @Basic
    @Column(name = "surname", length = 30, nullable = false)
    @Pattern(regexp = "^([a-zA-Z-]|[а-яА-Я-]){2,20}$", message = "")
    private String surname;

    @Basic
    @Column(name = "birthday", nullable = false)
    @PastOrPresent(message = "")
    private LocalDate birthday;

    @Basic
    @Column(name = "login", length = 30, unique = true, nullable = false)
    @Pattern(regexp = "^[A-Za-z][A-Za-z\\d_]{5,19}$", message = "")
    private String login;

    @Basic
    @Column(name = "password", length = 50, nullable = false)
    @Pattern(regexp = "^(?=.*[\\d])(?=.*[a-z])(?=.*[A-Z])(?=\\S+$).{8,20}$", message = "")
    private String password;

    @Basic
    @Column(name = "email", length = 30, unique = true, nullable = false)
    @Email(regexp = "^[A-Za-z\\d._%+-]+@[A-Za-z\\d.-]+\\.[A-Za-z]{2,6}$", message = "")
    private String email;

    public User(String name, String surname, LocalDate birthday, String login,
                String password, String email) {
        this.name = name;
        this.surname = surname;
        this.birthday = birthday;
        this.login = login;
        this.password = password;
        this.email = email;
    }
}
