package by.hurman.entity;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.Pattern;
import java.io.Serializable;
import java.util.Set;

@Entity
@Table(name = "books")
@Data
@NoArgsConstructor
public class Book implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_book")
    private int id;

    @Basic
    @Column(name = "title", length = 100, nullable = false)
    @Pattern(regexp = "", message = "")
    private String title;

    @Basic
    @Column(name = "year", nullable = false)
    @Pattern(regexp = "", message = "")
    private int year;

    @Basic
    @Column(name = "quantity", nullable = false)
    @Pattern(regexp = "", message = "")
    private int quantity;

    @Basic
    @Column(name = "image_link")
    @Pattern(regexp = "", message = "")
    private String imageLink;

    @ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinTable(name = "books_has_authors",
            joinColumns = @JoinColumn(name = "id_book"),
            inverseJoinColumns = @JoinColumn(name = "id_author"))
    private Set<Author> authors;

    public Book(String title, int year, int quantity,
                String imageLink, Set<Author> authors) {
        this.title = title;
        this.year = year;
        this.quantity = quantity;
        this.imageLink = imageLink;
        this.authors = authors;
    }
}