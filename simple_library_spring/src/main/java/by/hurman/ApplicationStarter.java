package by.hurman;

import by.hurman.dao.config.DatabaseConfig;
import by.hurman.dao.config.HibernateSession;
import by.hurman.dao.entity_dao.BookDao;
import by.hurman.entity.Book;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class ApplicationStarter {

    public static void main(String... args) {
        ApplicationContext ctx = new AnnotationConfigApplicationContext(DatabaseConfig.class,
                HibernateSession.class/* WebConfig.class*/);
        BookDao bookDao = ctx.getBean("bookDaoImpl", BookDao.class);
        for (Book book : bookDao.findAll()) {
            System.out.println(book);
        }
    }
}
