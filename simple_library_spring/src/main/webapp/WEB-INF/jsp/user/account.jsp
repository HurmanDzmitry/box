<%@ page contentType="text/html;charset=UTF-8" %>
<%@ page import="by.hurman.controller.util.Uri" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <jsp:include page="../part/link.jsp"/>
    <title>Account</title>
</head>
<body>
<header>
    <jsp:include page="../part/header.jsp"/>
</header>
<c:choose>
    <c:when test="${user != null}">
        <h3>${head}</h3>
        <br>
        Name: ${user.name_user} <br>
        Surname: ${user.surname_user} <br>
        Login: ${user.login} <br>
        Birthday: ${user.date_of_birth} <br>
        <br>
        <form action="${Uri.LOG_OUT}" method="post">
            <input type="submit" value="Log out"/>
        </form>
    </c:when>
    <c:otherwise>
        You are not logged in yet!<br>
    </c:otherwise>
</c:choose>
<jsp:include page="../part/footer.jsp"/>
</body>
</html>
