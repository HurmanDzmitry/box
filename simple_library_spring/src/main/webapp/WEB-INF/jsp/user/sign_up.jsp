<%@ page contentType="text/html;charset=UTF-8" %>
<%@ page import="by.hurman.controller.util.ParameterName" %>
<%@ page import="by.hurman.controller.util.Uri" %>
<%@ page import="java.time.LocalDate" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <jsp:include page="../part/link.jsp"/>
    <title>Registration</title>
</head>
<body>
<header>
    <jsp:include page="../part/header.jsp"/>
</header>
<h3>Registration</h3>
<br>
<c:choose>
    <c:when test="${sessionScope.id== null}">
        <c:if test="${not empty requestScope.message}">
            Registration failed<br>
            ${requestScope.message}
        </c:if>
        <form method="post" action="${Uri.SIGN_UP}">
            <label>Name:
                <input type="text" name="${ParameterName.NAME}" required><br>
            </label>
            <label>Surname:
                <input type="text" name="${ParameterName.SURNAME}" required><br>
            </label>
            <label>Birthday:
                <input type="date" name="${ParameterName.BIRTHDAY}"
                       max="${LocalDate.now().minusYears(5)}" min="${LocalDate.now().minusYears(100)}" required><br>
            </label>
            <label>Login:
                <input type="text" name="${ParameterName.LOGIN}" required><br>
            </label>
            <label>Password:
                <input type="password" name="${ParameterName.PASSWORD}" required><br>
            </label>
            <label>Password again:
                <input type="password" name="${ParameterName.PASSWORD_REPLAY}" required><br>
            </label>
            <label>Email:
                <input type="text" name="${ParameterName.EMAIL}" required><br>
            </label>
            <br>
            <p>
                <button type="submit">Submit</button>
            </p>
        </form>
    </c:when>
    <c:otherwise>
        You are already logged in!<br>
    </c:otherwise>
</c:choose>
<jsp:include page="../part/footer.jsp"/>
</body>
</html>


