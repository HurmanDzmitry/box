<%@ page contentType="text/html;charset=UTF-8" %>
<%@ page import="by.hurman.controller.util.ParameterName" %>
<%@ page import="by.hurman.controller.util.Uri" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <jsp:include page="../part/link.jsp"/>
    <title>Log in</title>
</head>
<body>
<header>
    <jsp:include page="../part/header.jsp"/>
</header>
<h3>Log in</h3>
<br>
<c:choose>
    <c:when test="${sessionScope.id == null}">
        <c:if test="${requestScope.message==true}">
            Wrong login or password!<br>
        </c:if>
        <form method="post" action="${Uri.LOG_IN}">
            <label>Login:
                <input type="text" name="${ParameterName.LOGIN}" required><br>
            </label>
            <label>Password:
                <input type="password" name="${ParameterName.PASSWORD}" required><br>
            </label>
            <button type="submit">Submit</button>
        </form>
        <br>
        <form action="${Uri.SIGN_UP_BLANK}">
            <button>Sign up</button>
        </form>
    </c:when>
    <c:otherwise>
        You are already logged in!<br>
    </c:otherwise>
</c:choose>
<jsp:include page="../part/footer.jsp"/>
</body>
</html>
