<%@ page contentType="text/html;charset=UTF-8" %>
<%@ page import="by.hurman.controller.util.ParameterName" %>
<%@ page import="by.hurman.controller.util.Uri" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<a href="${Uri.MAIN}" title="Back to main" class="logo">Library</a>
<span class="head">
    | <a href="${Uri.BOOKS}" title="Books">Books</a>
    |
</span>
<div class="right">
    <div class="head">
        <c:choose>
            <c:when test="${sessionScope.get(ParameterName.ID)!= null}">
                <a href="${Uri.ACCOUNT}" title="Account">Account</a>
            </c:when>
            <c:otherwise>
                <a href="${Uri.LOG_IN}" title="Log in">Log in</a>
            </c:otherwise>
        </c:choose>
    </div>
</div>
