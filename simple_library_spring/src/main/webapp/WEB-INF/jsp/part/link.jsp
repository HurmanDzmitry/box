<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<meta content="text/html" charset="UTF-8">
<link href="<c:url value="css/style.css"/>" rel="stylesheet" type="text/css">
<link href="<c:url value="img/title.ico"/>" rel="shortcut icon" type="image/x-icon">