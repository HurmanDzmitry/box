<%@ page contentType="text/html;charset=UTF-8" %>
<%@ page import="by.hurman.controller.util.ParameterName" %>
<%@ page import="by.hurman.controller.util.Uri" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <jsp:include page="../part/link.jsp"/>
    <title>Add book</title>
    <script>
        var countOfFields = 1;
        var curFieldNameId = 1;
        var maxFieldLimit = 10;

        function deleteField(a) {
            if (countOfFields > 1) {
                var contDiv = a.parentNode;
                contDiv.parentNode.removeChild(contDiv);
                countOfFields--;
            }
            return false;
        }

        function addField() {
            if (countOfFields >= maxFieldLimit) {
                return false;
            }
            countOfFields++;
            curFieldNameId++;
            var div = document.createElement("div");
            div.innerHTML = "<nobr><input list=\"author\" name=\"id" + curFieldNameId + "\" type=\"text\" required/> <datalist id=\"author\"> </datalist> <a style=\"color:red;\" onclick=\"return deleteField(this)\" href=\"#\">[-]</a> <a style=\"color:green;\" onclick=\"return addField()\" href=\"#\">[+]</a></nobr>";
            document.getElementById("parentId").appendChild(div);
            return false;
        }
    </script>
</head>
<body>
<header>
    <jsp:include page="../part/header.jsp"/>
</header>
<h3>Adding book</h3>
<c:if test="${not empty requestScope.message}">
    Book doesn't add.<br>
    ${requestScope.message}
</c:if>
<br>
<form method="post" action="${Uri.ADD_BOOK}" enctype="multipart/form-data">
    <label>Title:
        <input type="text" name="${ParameterName.TITLE}" required><br>
    </label>
    <label>Year:
        <input type="number" name="${ParameterName.YEAR}" required><br>
    </label>
    <label>Quantity:
        <input type="number" name="${ParameterName.QUANTITY}" required><br>
    </label>
    <label>Image:
        <input type="file" name="image" accept="image/*"><br>
    </label>
    <div id="parentId">
        <label>Author(s):
            <nobr><input list="authors" name="${ParameterName.ID}1" type="text" required/>
                <datalist id="authors">
                    <c:forEach var="author" items="${authors}">
                    <option id="${author.id_author}" value="${author.id_author}"><label
                            for="${author.id_author}">${author.name_author}</label>
                        </c:forEach>
                </datalist>
                <a style="color:red;" onclick="return deleteField(this)" href="#">[-]</a>
                <a style="color:green;" onclick="return addField()" href="#">[+]</a>
            </nobr>
        </label>
    </div>
    <br>
    <button type="submit">Submit</button>
</form>
<jsp:include page="../part/footer.jsp"/>
</body>
</html>
