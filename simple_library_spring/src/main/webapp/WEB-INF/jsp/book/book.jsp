<%@ page contentType="text/html;charset=UTF-8" %>
<%@ page import="by.hurman.controller.util.Uri" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <jsp:include page="../part/link.jsp"/>
    <title>Book</title>
</head>
<body>
<header>
    <jsp:include page="../part/header.jsp"/>
</header>
<br>
<table border='0' cellpadding='1' width='50%'>
    <tr>
        <c:set var="book" value="${book}"/>
        <td>
            <p><img src="${book.imageLink}" alt="${book.title}" width="200"></p>
        </td>
        <td>
            ${book.title}<br><br>
            <c:forEach var="author" items="${book.authors}">
                <a href="${Uri.AUTHOR}?id=${author.id_author}"
                   style="color:black; text-decoration:underline">${author.name_author}, </a>
            </c:forEach>
            ${book.year}
        </td>
    </tr>
</table>
<jsp:include page="../part/footer.jsp"/>
</body>
</html>
