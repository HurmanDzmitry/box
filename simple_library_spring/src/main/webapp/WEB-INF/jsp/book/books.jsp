<%@ page contentType="text/html;charset=UTF-8" %>
<%@ page import="by.hurman.controller.util.ParameterName" %>
<%@ page import="by.hurman.controller.util.Uri" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <jsp:include page="../part/link.jsp"/>
    <title>Books</title>
</head>
<body>
<header>
    <jsp:include page="../part/header.jsp"/>
</header>
<form action="${Uri.ADD_BOOK_BLANK}" class="right">
    <button>Add book</button>
</form>
<c:set var="numCols" value="3"/>
<c:set var="numRows" value="3"/>
<c:set var="rowCount" value="0"/>
<table width='100%' cellspacing="10px" style="table-layout: fixed;">
    <caption><h3>Books</h3></caption>
    <colgroup>
        <col width="10%">
        <col width="23%">
        <col width="10%">
        <col width="23%">
        <col width="10%">
        <col width="23%">
    </colgroup>
    <tr>
        <c:forEach var="book" items="${books}" varStatus="status" end="${on_page-1}">
        <c:if test="${rowCount lt numRows}">
        <td>
            <a href="${Uri.BOOK}/${book.id}">
                <img src="${book.imageLink}" alt="${book.title}" width="100%"></a>
        </td>
        <td>
                ${book.title}<br><br>
            <c:forEach var="author" items="${book.authors}">
                <a href="${Uri.AUTHOR}/${author.id}"
                   style="color:black; text-decoration:underline">${author.name}</a>
            </c:forEach>
            , ${book.year}
        </td>
        <c:if test="${status.count ne 0 && status.count % numCols == 0}">
        <c:set var="rowCount" value="${rowCount + 1}"/>
    </tr>
    <tr>
        </c:if>
        </c:if>
        </c:forEach>
    </tr>
</table>
<span>
<c:forEach begin="1" end="${pages}" varStatus="loop">
    | <a
        href="${Uri.BOOKS}?${ParameterName.PAGE}=${loop.current}&${requestScope['javax.servlet.forward.query_string'].replaceFirst("page=[\\d]+&?","")}"
        style="color: black; font-size: 1.5em;">${loop.current}</a>
</c:forEach>|
</span>
<jsp:include page="../part/footer.jsp"/>
</body>
</html>
