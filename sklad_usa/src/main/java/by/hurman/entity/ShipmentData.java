package by.hurman.entity;

import java.util.Objects;

public class ShipmentData {

    private String customer;
    private String company;
    private String address;
    private String city;
    private String zip;
    private String state;
    private String phone;
    private String comment;
    private String quantity;
    private String orderId;
    private String weight;
    private String height;
    private String width;
    private String length;

    public ShipmentData(String customer, String company, String address,
                        String city, String zip, String state, String phone,
                        String comment, String quantity, String orderId, String weight,
                        String height, String width, String length) {
        this.customer = customer;
        this.company = company;
        this.address = address;
        this.city = city;
        this.zip = zip;
        this.state = state;
        this.phone = phone;
        this.comment = comment;
        this.quantity = quantity;
        this.orderId = orderId;
        this.weight = weight;
        this.height = height;
        this.width = width;
        this.length = length;
    }

    public String getCustomer() {
        return customer;
    }

    public String getCompany() {
        return company;
    }

    public String getAddress() {
        return address;
    }

    public String getCity() {
        return city;
    }

    public String getZip() {
        return zip;
    }

    public String getState() {
        return state;
    }

    public String getPhone() {
        return phone;
    }

    public String getComment() {
        return comment;
    }

    public String getQuantity() {
        return quantity;
    }

    public String getOrderId() {
        return orderId;
    }

    public String getWeight() {
        return weight;
    }

    public String getHeight() {
        return height;
    }

    public String getWidth() {
        return width;
    }

    public String getLength() {
        return length;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ShipmentData that = (ShipmentData) o;
        return Objects.equals(customer, that.customer) &&
                Objects.equals(company, that.company) &&
                Objects.equals(address, that.address) &&
                Objects.equals(city, that.city) &&
                Objects.equals(zip, that.zip) &&
                Objects.equals(state, that.state) &&
                Objects.equals(phone, that.phone) &&
                Objects.equals(comment, that.comment) &&
                Objects.equals(quantity, that.quantity) &&
                Objects.equals(orderId, that.orderId) &&
                Objects.equals(weight, that.weight) &&
                Objects.equals(height, that.height) &&
                Objects.equals(width, that.width) &&
                Objects.equals(length, that.length);
    }

    @Override
    public int hashCode() {
        return Objects.hash(customer, company, address, city, zip, state, phone,
                comment, quantity, orderId, weight, height, width, length);
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + "{" +
                "customer='" + customer + '\'' +
                ", company='" + company + '\'' +
                ", address='" + address + '\'' +
                ", city='" + city + '\'' +
                ", zip='" + zip + '\'' +
                ", state='" + state + '\'' +
                ", phone='" + phone + '\'' +
                ", comment='" + comment + '\'' +
                ", quantity='" + quantity + '\'' +
                ", orderId='" + orderId + '\'' +
                ", weight='" + weight + '\'' +
                ", height='" + height + '\'' +
                ", width='" + width + '\'' +
                ", length='" + length + '\'' +
                '}';
    }
}