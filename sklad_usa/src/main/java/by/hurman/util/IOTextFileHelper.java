package by.hurman.util;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Map;

public class IOTextFileHelper {

    public static void writeToFile(Map<String, String> report, String fileName) throws IOException {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy_MM_dd_HH_mm_ss");
        String formattedDate = formatter.format(LocalDateTime.now());
        BufferedWriter writer = new BufferedWriter(new FileWriter(fileName + formattedDate + ".txt"));

        StringBuilder out = new StringBuilder();
        report.keySet().forEach(key -> out.append(key)
                .append(" - ")
                .append(report.get(key))
                .append("\n")
        );
        writer.write(out.toString() + "\nCount: " + report.size());
        writer.close();
    }
}