package by.hurman.util;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class PropertiesHelper {

    public static String getParam(String key) {
        try (InputStream input = new FileInputStream("params.properties")) {
            Properties prop = new Properties();
            prop.load(input);
            return prop.getProperty(key);
        } catch (
                IOException ex) {
            throw new RuntimeException("Param not get");
        }
    }
}