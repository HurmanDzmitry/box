package by.hurman.page;

import static com.codeborne.selenide.Selenide.title;

public class DashboardPage {

    private static final String EXPECTED_TITLE = "SkladUsa.com | Dashboard";

    public static boolean isOpened() {
        return EXPECTED_TITLE.equals(title());
    }
}