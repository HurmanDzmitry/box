package by.hurman.page;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$x;
import static com.codeborne.selenide.Selenide.title;

public class ShipmentListPage {

    private static final String EXPECTED_TITLE = "SkladUsa.com |";

    private static final String SUCCESSFUL_CREATION_GREEN_MESSAGE_ELEMENT_LOCATOR = "//div[@class='alert alert-success alert-dismissable']";
    private static final String CREATE_LABEL_LINK_LOCATOR = "//a[@href='/ru/admin/orders-self-label-create/create']";

    public static SelenideElement getSuccessfulMessageElement() {
        return $x(SUCCESSFUL_CREATION_GREEN_MESSAGE_ELEMENT_LOCATOR);
    }

    public static SelenideElement getCreateLabelLink() {
        return $x(CREATE_LABEL_LINK_LOCATOR);
    }

    public static boolean isOpened() {
        return EXPECTED_TITLE.equals(title());
    }

    public static void clickCreateLabelLink() {
        getCreateLabelLink().click();
    }
}