package by.hurman.page;

import by.hurman.entity.ShipmentData;

import java.time.Duration;
import java.util.Objects;

import static com.codeborne.selenide.Condition.matchText;
import static com.codeborne.selenide.Condition.value;
import static com.codeborne.selenide.Selenide.$x;

public class CreateShipmentPage {

    private static String PREFIX_CODE = "";
    private static final String HIDDEN_ANCHOR_ELEMENT_LOCATOR = "//input[@type='hidden' and @class=' form-control']";
    private static final String INPUT_ID_LOCATOR_PATTERN = "//input[@id='%s']";
    private static final String TEXTAREA_ID_LOCATOR_PATTERN = "//textarea[@id='%s']";

    private static final String CUSTOMER_FIELD_POSTFIX = "_customer";
    private static final String COMPANY_NAME_FIELD_POSTFIX = "_userCompanyName";
    private static final String ADDRESS_FIELD_POSTFIX = "_addressPart";
    private static final String CITY_FIELD_POSTFIX = "_city";
    private static final String ZIP_CODE_FIELD_POSTFIX = "_zip";
    private static final String STATE_FIELD_POSTFIX = "_userState";
    private static final String PHONE_FIELD_POSTFIX = "_userPhone";
    private static final String COMMENT_FIELD_POSTFIX = "_comment";
    private static final String ITEM_NUMBER_FIELD_POSTFIX = "_lotNumber";
    private static final String WEIGHT_OZ_FIELD_POSTFIX = "_weightOz";
    private static final String HEIGHT_FIELD_POSTFIX = "_size1";
    private static final String WIDTH_FIELD_POSTFIX = "_size2";
    private static final String LENGTH_FIELD_POSTFIX = "_size3";
    private static final String CONTENT_CATEGORY_DROPDOWN_LOCATOR = "//a[@class='select2-choice']/span[contains(text(),'Choose an option')]/..";
    private static final String CONTENT_FROM_SAVED_PROFILES_OPTION_LOCATOR = "//div[@class='select2-result-label' and contains(text(),'From saved profiles')]";
    private static final String CONTENT_PRODUCT_NAME_EN_POSTFIX = "_productConsolidation_0_nameEn";
    private static final String DELIVERY_METHOD_UNDER_lINK_LOCATOR = "//a[@id='updateShipStationServices']";
    private static final String DELIVERY_METHOD_FIELD_LOCATOR = "//div[@id='s2id_idSelectShipStationServiceCode']/a/span[@class='select2-chosen']";
    private static final String CREATE_BUTTON_LOCATOR = "//button[@name='btn_create_and_list']";

    public static boolean isOpened() {
        PREFIX_CODE = getPrefixCode();
        return $x(generateInputLocator(CUSTOMER_FIELD_POSTFIX)).exists() &&
                $x(DELIVERY_METHOD_UNDER_lINK_LOCATOR).exists() &&
                $x(generateLocator(TEXTAREA_ID_LOCATOR_PATTERN, COMMENT_FIELD_POSTFIX)).exists();
    }

    public static void fillForm(ShipmentData data) {
        PREFIX_CODE = getPrefixCode();
        $x(generateInputLocator(CUSTOMER_FIELD_POSTFIX)).sendKeys(data.getCustomer());
        $x(generateInputLocator(COMPANY_NAME_FIELD_POSTFIX)).sendKeys(data.getCompany());
        $x(generateInputLocator(ADDRESS_FIELD_POSTFIX)).sendKeys(data.getAddress());
        $x(generateInputLocator(CITY_FIELD_POSTFIX)).sendKeys(data.getCity());
        $x(generateInputLocator(ZIP_CODE_FIELD_POSTFIX)).sendKeys(data.getZip());
        $x(generateInputLocator(STATE_FIELD_POSTFIX)).sendKeys(data.getState());
        $x(generateInputLocator(PHONE_FIELD_POSTFIX)).sendKeys(data.getPhone());
        $x(generateLocator(TEXTAREA_ID_LOCATOR_PATTERN, COMMENT_FIELD_POSTFIX)).sendKeys(data.getComment());
        $x(generateInputLocator(ITEM_NUMBER_FIELD_POSTFIX)).sendKeys(data.getOrderId());
        $x(generateInputLocator(WEIGHT_OZ_FIELD_POSTFIX)).sendKeys(data.getWeight());
        $x(generateInputLocator(HEIGHT_FIELD_POSTFIX)).sendKeys(data.getHeight());
        $x(generateInputLocator(WIDTH_FIELD_POSTFIX)).sendKeys(data.getWidth());
        $x(generateInputLocator(LENGTH_FIELD_POSTFIX)).sendKeys(data.getLength());

        $x(CONTENT_CATEGORY_DROPDOWN_LOCATOR).click();
        $x(CONTENT_FROM_SAVED_PROFILES_OPTION_LOCATOR).click();
        $x(generateInputLocator(CONTENT_PRODUCT_NAME_EN_POSTFIX)).shouldHave(value("STICKER"), Duration.ofSeconds(60));
        $x(DELIVERY_METHOD_UNDER_lINK_LOCATOR).click();
        $x(DELIVERY_METHOD_FIELD_LOCATOR).shouldHave(matchText("\\w+"), Duration.ofSeconds(60));
    }

    public static void clickCreateButton() {
        $x(CREATE_BUTTON_LOCATOR).click();
    }

    private static String getPrefixCode() {
        return Objects.requireNonNull($x(HIDDEN_ANCHOR_ELEMENT_LOCATOR).getAttribute("id")).split("_")[0];
    }

    private static String generateInputLocator(String postfix) {
        return generateLocator(INPUT_ID_LOCATOR_PATTERN, postfix);
    }

    private static String generateLocator(String pattern, String postfix) {
        return String.format(pattern, PREFIX_CODE + postfix);
    }
}