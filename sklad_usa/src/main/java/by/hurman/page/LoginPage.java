package by.hurman.page;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.title;

public class LoginPage {

    private static final String EXPECTED_TITLE = "System | SkladUsa.com";

    private static final String EMAIL_FIELD_SELECTOR = "input[id='username']";
    private static final String PASSWORD_FIELD_SELECTOR = "input[id='password']";
    private static final String SIGN_IN_BUTTON_SELECTOR = "button[id='_submit']";

    public static SelenideElement getEmailField() {
        return $(EMAIL_FIELD_SELECTOR);
    }

    public static SelenideElement getPasswordField() {
        return $(PASSWORD_FIELD_SELECTOR);
    }

    public static SelenideElement getSignInButton() {
        return $(SIGN_IN_BUTTON_SELECTOR);
    }

    public static boolean isOpened() {
        return EXPECTED_TITLE.equals(title())
                && getEmailField().isDisplayed();
    }

    public static void signIn(String email, String password) {
        getEmailField().sendKeys(email);
        getPasswordField().sendKeys(password);
        getSignInButton().click();
    }
}