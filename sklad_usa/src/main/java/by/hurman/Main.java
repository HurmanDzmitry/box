package by.hurman;

import by.hurman.entity.ShipmentData;
import by.hurman.page.CreateShipmentPage;
import by.hurman.page.DashboardPage;
import by.hurman.page.LoginPage;
import by.hurman.page.ShipmentListPage;
import by.hurman.util.GoogleExcelHelper;
import by.hurman.util.IOTextFileHelper;
import by.hurman.util.PropertiesHelper;
import com.codeborne.selenide.Condition;
import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.ex.AlertNotFoundException;

import java.io.IOException;
import java.time.Duration;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.codeborne.selenide.Selenide.*;
import static org.hamcrest.MatcherAssert.assertThat;

public class Main {

    private static final String startUrl = PropertiesHelper.getParam("startUrl");
    private static final String email = PropertiesHelper.getParam("email");
    private static final String password = PropertiesHelper.getParam("password");
    private static final String googleTableIDFromUrl = PropertiesHelper.getParam("googleTableIDFromUrl");
    private static final String googleTableSheetName = PropertiesHelper.getParam("googleTableSheetName");
    private static final String googleTableRange = PropertiesHelper.getParam("googleTableRange");

    public static void main(String[] args) throws IOException {
        Configuration.startMaximized = true;

        open(startUrl);
        assertThat("Login page not opened", LoginPage.isOpened());

        LoginPage.signIn(email, password);
        assertThat("Dashboard page not opened", DashboardPage.isOpened());

        List<ShipmentData> dataList = GoogleExcelHelper.getData(googleTableIDFromUrl,
                googleTableSheetName,
                googleTableRange);
        Map<String, String> report = new HashMap<>();
        Map<String, String> log = new HashMap<>();
        for (ShipmentData data : dataList) {
            try {
                open("https://system.skladusa.com/ru/admin/orders-self-label-create/list");
                try {
                    switchTo().alert().accept();
                    report.put(data.getOrderId(), "Was alert");
                    open("https://system.skladusa.com/ru/admin/orders-self-label-create/list");
                } catch (AlertNotFoundException ex) {
                    report.put(data.getOrderId(), "No alert");
                }
                assertThat("List of shipment page not opened", ShipmentListPage.isOpened());
                ShipmentListPage.clickCreateLabelLink();
                assertThat("Page with form for create shipment not opened", CreateShipmentPage.isOpened());

                CreateShipmentPage.fillForm(data);
                CreateShipmentPage.clickCreateButton();
                ShipmentListPage.getSuccessfulMessageElement().shouldBe(Condition.visible, Duration.ofSeconds(60));

                report.put(data.getOrderId(), "true");
            } catch (Throwable e) {
                report.put(data.getOrderId(), "false");
                String screenshotFile = screenshot("save_"
                        + DateTimeFormatter.ofPattern("HH_mm_ss_SSS").format(LocalTime.now()));
                log.put(data.getOrderId(), screenshotFile + "\n" + Arrays.toString(e.getStackTrace()));
            }
        }
        IOTextFileHelper.writeToFile(report, "report_");
        IOTextFileHelper.writeToFile(log, "build\\reports\\tests\\log_");
        closeWebDriver();
    }
}