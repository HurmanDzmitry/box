package by.hurman;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;

import java.time.Duration;
import java.util.concurrent.TimeUnit;

class AppTest {

    private WebDriver driver;
    private WebElement element;
    private Wait<WebDriver> wait;

    @BeforeEach
    void init() {
        System.setProperty("webdriver.chrome.driver", ".\\src\\main\\resources\\chromedriver_81.exe");
        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.manage().window().maximize();
        driver.get("https://www.onliner.by");
    }

    @AfterEach
    void tearDown() {
        driver.quit();
    }

    @Test
    void isPriceMoreOneHundred() {
        driver.findElement(By.cssSelector("[href=\"https://catalog.onliner.by/mobile\"]")).click();

        element = driver.findElement(By.xpath("//*[@id=\"schema-filter\"]/div[3]/div[6]/div[2]/ul/li[1]/label/span[1]/span"));
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", element);
        element.click();

        wait = new FluentWait<>(driver)
                .withTimeout(Duration.ofMillis(10000))
                .pollingEvery(Duration.ofMillis(100))
                .ignoring(Exception.class);
        wait.until(dr -> dr.findElement(By.cssSelector("[class=\"schema-products schema-products_processing\"]")));
        wait.until(dr -> dr.findElement(By.cssSelector("[class=\"schema-products\"]")));
        element = driver.findElement(By.xpath("//*[@id=\"schema-products\"]/div[1]/div/div[3]/div[1]/div/div[1]/div[1]/a/span"));

        int x = Integer.parseInt(element.getAttribute("textContent").replaceAll("[^\\d]", ""));

        Assertions.assertTrue(100 < x / 100.0);
    }
}