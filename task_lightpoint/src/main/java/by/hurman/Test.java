package by.hurman;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class Test {

    public static void main(String[] args) {
        Map<Double, Integer> map = new HashMap<>();
        for (int i = 0; i < 10; i++) {
            double x = cleanVersion();
            int y = 0;
            if (map.containsKey(x)) {
                y = map.get(x);
            }
            map.put(x, ++y);
        }

        System.out.println(cleanVersion());
    }

    private static double getCost() {
        try {
            //        String driverPath = App.class.getClassLoader().getResource("") + "chromedriver_81.exe";
            System.setProperty("webdriver.chrome.driver", ".\\src\\main\\resources\\chromedriver_81.exe");

//        ChromeOptions options = new ChromeOptions();
//        options.addArguments("--force-device-scale-factor=1");
//        options.addArguments("--disable-gpu");
//        options.addArguments("--no-sandbox");

            WebDriver driver = new ChromeDriver(/*options*/);
            WebElement element;
            WebDriverWait webDriverWait;

//            driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
            driver.manage().window().maximize();

            driver.get("https://www.onliner.by");

            driver.findElement(By.cssSelector("[href=\"https://catalog.onliner.by/mobile\"]")).click();

            webDriverWait = new WebDriverWait(driver, 10);
//            webDriverWait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("[class=\"schema-grid__left-column\"]")));

            element = driver.findElement(By.xpath("//*[@id=\"schema-filter\"]/div[3]/div[6]/div[2]/ul/li[1]/label/span[1]/span"));
            JavascriptExecutor jse = (JavascriptExecutor) driver;
            jse.executeScript("arguments[0].scrollIntoView(true);", element);
            element.click();
//        Actions action = new Actions(driver);
//        action.moveToElement(element).click().perform();

//надпись apple над списком
            webDriverWait.until(ExpectedConditions
                    .visibilityOfElementLocated(By.xpath("//*[@id=\"schema-tags\"]/div/span")));

            element = driver.findElement(By.xpath("//*[@id=\"schema-products\"]/div[1]/div/div[3]/div[1]/div/div[1]/div[1]/a/span"));
            int x = Integer.parseInt(element.getAttribute("textContent").replaceAll("[^\\d]", ""));
            driver.quit();
            return x / 100.0;
//        driver.close();
        } catch (Exception e) {
            return 0;
        }
    }

    private static double cleanVersion() {
        System.setProperty("webdriver.chrome.driver", ".\\src\\main\\resources\\chromedriver_81.exe");
        WebDriver driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.manage().window().maximize();
        driver.get("https://www.onliner.by");

        driver.findElement(By.cssSelector("[href=\"https://catalog.onliner.by/mobile\"]")).click();

        WebElement element = driver.findElement(By.xpath("//*[@id=\"schema-filter\"]/div[3]/div[6]/div[2]/ul/li[1]/label/span[1]/span"));
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", element);
        element.click();

        Wait<WebDriver> wait = new FluentWait<>(driver)
                .withTimeout(Duration.ofMillis(10000))
                .pollingEvery(Duration.ofMillis(100))
                .ignoring(Exception.class);
        wait.until(dr -> dr.findElement(By.cssSelector("[class=\"schema-products schema-products_processing\"]")));
        wait.until(dr -> dr.findElement(By.cssSelector("[class=\"schema-products\"]")));
        element = driver.findElement(By.xpath("//*[@id=\"schema-products\"]/div[1]/div/div[3]/div[1]/div/div[1]/div[1]/a/span"));

        double x = Integer.parseInt(element.getAttribute("textContent")
                .replaceAll("[^\\d]", "")) / 100.0;
        driver.quit();
        return x;
    }
}