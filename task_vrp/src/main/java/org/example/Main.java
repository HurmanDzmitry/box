package org.example;

import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.io.File;
import java.time.Duration;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

import static com.google.common.base.Predicates.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

public class Main {

    public static void main(String[] args) {
        System.out.println("Hello world!");

        int[] array = new int[]{9, 0, 2, 5, 1, 6, 3, -1};
        System.out.println(Arrays.toString(array));

        for (int i = 0; i < array.length; i++) {
            for (int j = i + 1; j < array.length; j++) {
                if (array[i] > array[j]) {
                    int temp = array[i];
                    array[i] = array[j];
                    array[j] = temp;
                }
            }
        }

        System.out.println(Arrays.toString(array));
    }

    private WebDriver driver;

    @BeforeClass
    public void setup() {
        System.setProperty("webdriver.chrome.driver", new File("src/main/resources/chromedriver.mac").getAbsolutePath());
        driver = new ChromeDriver();
//        WebDriverManager.firefoxdriver().setup(); garcia lib
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(20)); // Implicity wait

        ChromeOptions chromeOptions = new ChromeOptions();
        chromeOptions.setPageLoadStrategy(PageLoadStrategy.NORMAL);
        driver = new ChromeDriver(chromeOptions);
    }

    @Test
    public void test() {
//public void test(ITestContext context) {
//        String value = context.getCurrentXmlTest().getParameter("url");
        HomePage homePage = new HomePage(driver);
        SearchResultsPage searchResultsPage = new SearchResultsPage(driver);

        driver.get("http://help.salesforce.com/");
        driver.navigate().to("http://help.salesforce.com/");

        Set<Cookie> cookies = driver.manage().getCookies(); // cookies
        driver.manage().getCookieNamed("cooookie"); // cookies
        driver.manage().addCookie(new Cookie("", "")); // cookies

        driver.switchTo().alert();
        driver.switchTo().window("blabla");
        driver.switchTo().newWindow(WindowType.TAB);//new tab
        driver.switchTo().frame(1);

//        Assert.assertTrue(searchResultsPage.isOpened());

        homePage.getSearchResourcesField().sendKeys(Keys.chord(Keys.CONTROL, "a"), Keys.DELETE);
        homePage.getSearchResourcesField().clear();

//        homePage.searchResources("test");
        homePage.getSearchResourcesField().sendKeys("test");
        homePage.selectSuggestedOption("test class");
        Assert.assertTrue(searchResultsPage.isOpened());
//        Assert.assertEquals(searchResultsPage.isOpened());
//        Assert.(searchResultsPage.isOpened());

        Actions actions = new Actions(driver);
        actions.click();
        actions.perform();
        actions.click();

        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(30));
        wait.until(Boolean -> 10 % 5 == 0);

        Wait<WebDriver> qqq = new FluentWait<>(driver)
                .withTimeout(Duration.ofSeconds(30))
                .pollingEvery(Duration.ofSeconds(1));
//        wait.until(ExpectedConditions.visibilityOf());
//        wait.until(ExpectedConditions.presenceOfElementLocated());

        searchResultsPage.selectOptionsInContentTypeSection(List.of("Videos", "Answers"));
//        Assert.assertTrue(searchResultsPage.isContainOnlyTypes());

        assertThat("Fail here", 11, equalTo(11));
    }


    @AfterClass
    public void tearDown() {
        driver.quit();
    }

    private void clickElement(WebElement element) {
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(5));
        wait.until(ExpectedConditions.elementToBeClickable(By.className("nav--menu_item_anchor")));
        JavascriptExecutor executor = (JavascriptExecutor) driver;
        executor.executeScript("arguments[0].click();", element);
    }
}