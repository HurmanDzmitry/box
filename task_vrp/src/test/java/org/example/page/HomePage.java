package org.example.page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import java.util.List;

public class HomePage extends BasePage {

    public static final String TITLE = "Salesforce Help | Home";

    private final WebDriver driver;

    @FindBy(xpath = "//input[@placeholder='Search documents, videos, and help resources']")
    private WebElement searchResourcesField;
    @FindBy(xpath = "//div[@id='standaloneSearchbox']//div[@role='button' and @aria-label='Search']")
    private WebElement searchResourcesButton;
    @FindBy(xpath = "//div[contains(@class, 'magic-box-hasSuggestion')]/div[contains(@id, 'coveo-magicbox-suggestions')]")
    private WebElement searchResourcesDropdown;

    public HomePage(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }

    @Override
    public boolean isOpened() {
        String title = driver.getTitle();
        return TITLE.equals(title);
    }

    public WebElement getSearchResourcesField() {
        return searchResourcesField;
    }

    public WebElement getSearchResourcesButton() {
        return searchResourcesButton;
    }

    public WebElement getSearchResourcesDropdown() {
        return searchResourcesDropdown;
    }

    public void selectSuggestedOption(String option) {
        Assert.assertTrue(searchResourcesDropdown.isDisplayed());
        List<WebElement> elements = searchResourcesDropdown.findElements(By.xpath("div[@role='option']"));
        WebElement suggestedOption = elements.stream()
                .filter(e -> option.equals(e.getAttribute("aria-label")))
                .findFirst()
                .orElseThrow(() -> new RuntimeException("No option"));
        suggestedOption.click();
    }
}