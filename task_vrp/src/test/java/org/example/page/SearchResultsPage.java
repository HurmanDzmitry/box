package org.example.page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class SearchResultsPage extends BasePage {

    public static final String TITLE = "Salesforce Help | Search Results";

    private final WebDriver driver;

    @FindBy(id = "objecttypeFacet")
    private WebElement contentTypeSection;
    @FindBy(className = "coveo-result-list-container")
    private WebElement resultList;

    public SearchResultsPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }

    public boolean isOpened() {
        String title = driver.getTitle();
        return TITLE.equals(title);
    }

    public WebElement getContentTypeSection() {
        return contentTypeSection;
    }

    public WebElement getResultList() {
        return resultList;
    }

    public void selectOptionsFromContentTypeSection(Set<String> options) {
        options.forEach(o -> {
            WebElement checkbox = getContentTypeSection()
                    .findElement(By.xpath(String.format("ul/li//button[contains(@aria-label, '%s')]", o)));
            if ("false".equals(checkbox.getAttribute("aria-pressed"))) {
                checkbox.click();
            }
        });
    }

    public boolean isFilteredByTypes(Set<String> options) {
        List<WebElement> resultCells = getResultList()
                .findElements(By.className("CoveoResult"));
        Set<String> contentTypes = resultCells.stream()
                .map(i -> i.findElement(By.className("CoveoText")).getText())
                .collect(Collectors.toSet());
        return options.containsAll(contentTypes);
    }
}