package org.example.test;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.example.page.HomePage;
import org.example.page.SearchResultsPage;
import org.openqa.selenium.Keys;
import org.openqa.selenium.PageLoadStrategy;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.Assert;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

import java.time.Duration;
import java.util.Set;

public class InitialTest {

    private WebDriver driver;

    @BeforeSuite
    public void setup() {
        ChromeOptions options = new ChromeOptions();
        options.setPageLoadStrategy(PageLoadStrategy.NORMAL);
        options.setImplicitWaitTimeout(Duration.ofSeconds(20));
        options.addArguments("--start-maximized");
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver(options);
    }

    @AfterSuite
    public void tearDown() {
        driver.quit();
    }

    @Test
    public void test() {
        HomePage homePage = new HomePage(driver);
        SearchResultsPage searchResultsPage = new SearchResultsPage(driver);

        driver.get("http://help.salesforce.com/");
        Assert.assertTrue(homePage.isOpened());

        homePage.getSearchResourcesField().clear();
        homePage.getSearchResourcesField().sendKeys("test");
        homePage.getSearchResourcesField().sendKeys(Keys.ENTER);
        Assert.assertTrue(searchResultsPage.isOpened());

        searchResultsPage.waitPageLoad(driver);
        searchResultsPage.selectOptionsFromContentTypeSection(Set.of("Videos", "Answers"));
        searchResultsPage.getWait(driver)
                .withMessage("Bad")
                .ignoring(StaleElementReferenceException.class)
                .until(Boolean -> searchResultsPage.isFilteredByTypes(Set.of("Video", "Answers")));
    }
}