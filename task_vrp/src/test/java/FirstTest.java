import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import java.time.Duration;

public class FirstTest {

    private WebDriver driver;

    @BeforeSuite
    public void setUp() {
        WebDriverManager.chromedriver().setup();
        ChromeOptions options = new ChromeOptions();
        options.setImplicitWaitTimeout(Duration.ofSeconds(20));
        driver = new ChromeDriver();
    }

    @Test
    public void firstTest() {
        driver.get("https://help.salesforce.com/");

        By xpath = By.xpath("//input[@placeholder='Search documents, videos, and help resources']");
        new WebDriverWait(driver, Duration.ofSeconds(10))
//                .pollingEvery(Duration.ofSeconds(1))
                .until(ExpectedConditions.presenceOfElementLocated(xpath));
        WebElement searchField = driver.findElement(xpath);

        searchField.clear();
        searchField.sendKeys("sales");
        searchField.sendKeys(Keys.ENTER);

        final String searchResultsTitle = "Salesforce Help | Search Results";
//        assertThat("", , equalTo(searchResultsTitle));

        SoftAssert a=new SoftAssert();
        a.assertAll();

        Assert.assertEquals(driver.getTitle(), searchResultsTitle);

        By xpath2 = By.xpath("(//div[@class='coveo-result-list-container coveo-list-layout-container']/div[1]/div[1]//a)[1]");
        new WebDriverWait(driver, Duration.ofSeconds(10))
//                .pollingEvery(Duration.ofSeconds(1))
                .until(ExpectedConditions.presenceOfElementLocated(xpath2));
        WebElement element2 = driver.findElement(xpath2);
        element2.click();

    }

    @AfterSuite
    public void tearDown() {
        driver.quit();
    }
}
