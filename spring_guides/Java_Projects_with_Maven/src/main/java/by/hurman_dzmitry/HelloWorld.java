package by.hurman_dzmitry;

import org.joda.time.LocalTime;

public class HelloWorld {
    public static void main(String[] args) {
        LocalTime currentTime = new LocalTime();
        System.out.println("The current local time is: " + currentTime);
        Greeter greeter = new Greeter();
        System.out.println(greeter.sayHello());
    }
}
//        start
//        mvn package
//        java -jar target/Java_Projects_with_Maven-1.0-SNAPSHOT.jar
