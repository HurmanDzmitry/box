package by.hurman.codewars;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class Chess {

    public static int knight(String start, String finish) {
        int startX = start.charAt(0) - 96;
        int startY = Integer.parseInt(start.substring(1));
        int finishX = finish.charAt(0) - 96;
        int finishY = Integer.parseInt(finish.substring(1));
        int[] begin = {startX, startY};
        int[] end = {finishX, finishY};
        Set<int[]> list;
        Set<int[]> nextList = new HashSet<>();
        int posX;
        int posY;
        int step = 0;
        int[] point = begin;
        nextList.add(point);
        while (nextList.stream().noneMatch(o -> Arrays.equals(o, end))) {
            list = new HashSet<>(nextList);
            nextList.clear();
            for (int[] ints : list) {
                posX = ints[0];
                posY = ints[1];
                if (posX + 2 < 9 && posY + 1 < 9) {
                    point = new int[]{posX + 2, posY + 1};
                    nextList.add(point);
                }
                if (posX + 1 < 9 && posY + 2 < 9) {
                    point = new int[]{posX + 1, posY + 2};
                    nextList.add(point);
                }
                if (posX + 2 < 9 && posY - 1 > 0) {
                    point = new int[]{posX + 2, posY - 1};
                    nextList.add(point);
                }
                if (posX + 1 < 9 && posY - 2 > 0) {
                    point = new int[]{posX + 1, posY - 2};
                    nextList.add(point);
                }
                if (posX - 1 > 0 && posY - 2 > 0) {
                    point = new int[]{posX - 1, posY - 2};
                    nextList.add(point);
                }
                if (posX - 2 > 0 && posY - 1 > 0) {
                    point = new int[]{posX - 2, posY - 1};
                    nextList.add(point);
                }
                if (posX - 1 > 0 && posY + 2 < 9) {
                    point = new int[]{posX - 1, posY + 2};
                    nextList.add(point);
                }
                if (posX - 2 > 0 && posY + 1 < 9) {
                    point = new int[]{posX - 2, posY + 1};
                    nextList.add(point);
                }
            }
            step++;
        }
        return step;
    }
}
