package by.hurman.codewars;

import java.math.BigInteger;

public class SumFct {

    public static BigInteger perimeter(BigInteger n) {
        return fib(n).multiply(BigInteger.valueOf(4));
    }

    static BigInteger fib(BigInteger n) {
        BigInteger sum = BigInteger.TWO;
        BigInteger a = BigInteger.ONE;
        BigInteger b = BigInteger.ONE;
        BigInteger c;
        if (n.equals(BigInteger.ZERO))
            return a;
        for (BigInteger i = BigInteger.TWO; i.compareTo(n) <= 0; i = i.add(BigInteger.ONE)) {
            c = a.add(b);
            a = b;
            b = c;
            sum = sum.add(b);
        }
        return sum;
    }
}