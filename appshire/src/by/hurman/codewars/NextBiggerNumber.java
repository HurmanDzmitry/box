package by.hurman.codewars;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class NextBiggerNumber {

    public static long nextBiggerNumber(long n) {
        List<Integer> numbers = new ArrayList<>();
        while (n > 0) {
            numbers.add((int) (n % 10));
            n = n / 10;
        }
        int[] arr = new int[numbers.size()];
        for (int i = 0; i < numbers.size(); i++) {
            arr[numbers.size() - i - 1] = numbers.get(i);
        }
        return findNext(arr, numbers.size());
    }

    private static void swap(int[] ar, int i, int j) {
        int temp = ar[i];
        ar[i] = ar[j];
        ar[j] = temp;
    }

    private static long findNext(int[] ar, int n) {
        int i;
        for (i = n - 1; i > 0; i--) {
            if (ar[i] > ar[i - 1]) {
                break;
            }
        }
        if (i == 0) {
            return -1;
        } else {
            int x = ar[i - 1];
            int min = i;
            for (int j = i + 1; j < n; j++) {
                if (ar[j] > x && ar[j] < ar[min]) {
                    min = j;
                }
            }
            swap(ar, i - 1, min);
            Arrays.sort(ar, i, n);
            StringBuilder answer = new StringBuilder();
            for (int l : ar) {
                answer.append(l);
            }
            return Long.valueOf(answer.toString());
        }
    }
}