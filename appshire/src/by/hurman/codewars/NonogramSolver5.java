package by.hurman.codewars;

import java.util.Arrays;

public class NonogramSolver5 {

    public static final int[][][] DATA = new int[][][]{{{1, 1}, {4}, {1, 1, 1}, {3}, {1}}, {{1}, {2}, {3}, {2, 1}, {4}}};

    public static void main(String[] args) {
        System.out.println(Arrays.deepToString(solve(DATA)));
    }

//    | 1 |   |   |
//    | 1 |   | 1 |   |   |
//    | 1 | 4 | 1 | 3 | 1 |
//1   |   |   | # |   |   |
//2   | # | # |   |   |   |
//3   |   | # | # | # |   |
//2 1 | # | # |   | # |   |
//4   |   | # | # | # | # |

    public static int[][] solve(int[][][] clues) {
        int[][] result = new int[5][5];
        Arrays.stream(result).forEach(i -> Arrays.fill(i, -1));



        return result;
    }
}
