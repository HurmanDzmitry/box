package by.hurman.codewars;

public class Greed {
    public static int greedy(int[] dice) {
        int[] map = new int[6];
        int side;
        int oldValue;
        for (int die : dice) {
            side = die;
            oldValue = map[side - 1];
            map[side - 1] = ++oldValue;
        }
        int sum = 0;
        for (int i = 0; i < map.length; i++) {
            if (map[i] >= 3) {
                if (i == 0) {
                    sum += 1000;
                } else {
                    sum += 100 * (i + 1);
                }
                map[i] = map[i] - 3;
                break;
            }
        }
        while (map[0] > 0) {
            sum += 100;
            map[0] = map[0] - 1;
        }
        while (map[4] > 0) {
            sum += 50;
            map[4] = map[4] - 1;
        }
        return sum;
    }
}
