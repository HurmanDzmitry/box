package by.hurman.codewars;

import java.awt.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class Finder {

    static List<String> cheapestPath(int[][] t, Point start, Point finish) {
        int width = t.length;
        int height = t[0].length;
        int[][] fields = new int[width][height];
        for (int i = 0; i < width; i++) {
            Arrays.fill(fields[i], Integer.MAX_VALUE);
        }
        List<Point> list;
        List<Point> nextList = new ArrayList<>();
        Point point;
        int posX;
        int posY;
        int cost;
        fields[start.x][start.y] = t[start.x][start.y];
        nextList.add(start);
        if (start.equals(finish)) {
            return Collections.emptyList();
        }
        while (nextList.size() > 0) {
            list = new ArrayList<>(nextList);
            nextList.clear();
            for (Point i : list) {
                point = i;
                posX = i.x;
                posY = i.y;
                cost = fields[posX][posY];
                if (cost < fields[finish.x][finish.y] && !point.equals(finish)) {
                    if (posX + 1 < width && fields[posX + 1][posY] > cost + t[posX + 1][posY]) {
                        fields[posX + 1][posY] = cost + t[posX + 1][posY];
                        nextList.add(new Point(posX + 1, posY));
                    }
                    if (posX - 1 >= 0 && fields[posX - 1][posY] > cost + t[posX - 1][posY]) {
                        fields[posX - 1][posY] = cost + t[posX - 1][posY];
                        nextList.add(new Point(posX - 1, posY));
                    }
                    if (posY + 1 < height && fields[posX][posY + 1] > cost + t[posX][posY + 1]) {
                        fields[posX][posY + 1] = cost + t[posX][posY + 1];
                        nextList.add(new Point(posX, posY + 1));
                    }
                    if (posY - 1 >= 0 && fields[posX][posY - 1] > cost + t[posX][posY - 1]) {
                        fields[posX][posY - 1] = cost + t[posX][posY - 1];
                        nextList.add(new Point(posX, posY - 1));
                    }
                }
            }
        }
        fields[start.x][start.y] = t[start.x][start.y];
        return getWay(fields, finish, start);
    }

    private static List<String> getWay(int[][] fields, Point finish, Point start) {
        List<String> way = new ArrayList<>();
        int[] arr = new int[4];
        arr[0] = Integer.MAX_VALUE;
        arr[1] = Integer.MAX_VALUE;
        arr[2] = Integer.MAX_VALUE;
        arr[3] = Integer.MAX_VALUE;
        while (!finish.equals(start)) {
            if (finish.x > 0) {
                arr[0] = fields[finish.x - 1][finish.y];
            }
            if (finish.x < fields.length - 1) {
                arr[1] = fields[finish.x + 1][finish.y];
            }
            if (finish.y > 0) {
                arr[2] = fields[finish.x][finish.y - 1];
            }
            if (finish.y < fields[0].length - 1) {
                arr[3] = fields[finish.x][finish.y + 1];
            }
            int min = Arrays.stream(arr).min().getAsInt();
            if (arr[0] == min) {
                way.add("down");
                finish.x = finish.x - 1;
            } else if (arr[1] == min) {
                way.add("up");
                finish.x = finish.x + 1;
            } else if (arr[2] == min) {
                way.add("right");
                finish.y = finish.y - 1;
            } else if (arr[3] == min) {
                way.add("left");
                finish.y = finish.y + 1;
            }
        }
        Collections.reverse(way);
        return way;
    }
}