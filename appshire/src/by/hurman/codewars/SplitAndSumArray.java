package by.hurman.codewars;

import java.util.Arrays;

public class SplitAndSumArray {
    public static int[] splitAndAdd(int[] numbers, int n) {
        while (numbers.length > 1 && n > 0) {
            int length = numbers.length;
            int border = length / 2;
            int newLength = length - border;
            int[] a;
            if (newLength > border) {
                a = new int[newLength];
                System.arraycopy(numbers, 0, a, 1, newLength - 1);
            } else {
                a = Arrays.copyOfRange(numbers, 0, border);
            }
            int[] b = Arrays.copyOfRange(numbers, border, length);
            numbers = new int[newLength];
            for (int i = 0; i < numbers.length; i++) {
                numbers[i] = a[i] + b[i];
            }
            n--;
        }
        return numbers;
    }
}