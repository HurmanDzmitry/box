package by.hurman.acmp;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Scanner;

public class Task_57 {
    public static void main(String[] args) throws FileNotFoundException {
        Scanner sc = new Scanner(new File("input.txt"));
        int n = sc.nextInt();
        int c = sc.nextInt();
        long p = sc.nextLong();
        int[][] points = new int[n + 1][2];
//        sum for center of gravity
        double sumX = 0;
        double sumY = 0;
        for (int i = 0; i < points.length; i++) {
            for (int j = 0; j < points[i].length; j++) {
                points[i][j] = sc.nextInt();
                if (j == 0)
                    sumX += points[i][j];
                else
                    sumY += points[i][j];
            }
        }
//        pig's center of gravity
        double x = (sumX - points[n][0]) / n;
        double y = (sumY - points[n][1]) / n;

        int indexNearPoint = getIndexNearPointToCentreOfGravity(points, x, y);

        double distance = getDistance(points, indexNearPoint);

        PrintWriter pw = new PrintWriter(new File("output.txt"));
        pw.print(getAnswer(c, p, distance));
        pw.close();
    }

    private static int getIndexNearPointToCentreOfGravity(int[][] array, double x, double y) {
        int index = 0;
        double minLength = Math.hypot(Math.abs(array[index][0] - x), Math.abs(array[index][1] - y));
        double actualLength;
        for (int i = 1; i < array.length - 1; i++) {
            actualLength = Math.hypot(Math.abs(array[i][0] - x), Math.abs(array[i][1] - y));
            if (actualLength < minLength) {
                minLength = actualLength;
                index = i;
            }
        }
        return index;
    }

    private static double getDistance(int[][] array, int index) {
//        distance between net point and first pig's point
        double distance = Math.hypot(Math.abs(array[index][0] - array[array.length - 1][0])
                , Math.abs(array[index][1] - array[array.length - 1][1]));
//        distance between first pig's point and other pig's point
        for (int i = 0; i < array.length - 1; i++) {
            if (i != index)
                distance += Math.hypot(Math.abs(array[i][0] - array[index][0])
                        , Math.abs(array[i][1] - array[index][1]));
        }
        return distance;
    }

    private static String getAnswer(int c, long p, double distance) {
        if (p >= c * distance)
            return "YES";
        else return "NO";
    }
}
