package by.hurman.acmp;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Scanner;

public class Task_175 {
    public static void main(String[] args) throws FileNotFoundException {
        Scanner sc = new Scanner(new File("input.txt"));
        String time = sc.nextLine();
        int h = Integer.parseInt(time.substring(0, 2));
        int m = Integer.parseInt(time.substring(3, 5));
        int tmp;
        if (h < 10) {
            tmp = 20;
        } else if (h < 17) {
            tmp = 24;
        } else if (h < 20) {
            tmp = 29;
        } else {
            tmp = 34;
        }
        PrintWriter pw = new PrintWriter(new File("output.txt"));
        pw.print(tmp * 60 - (h * 60 + m));
        pw.close();
    }
}
