package by.hurman.acmp;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

public class Task_99 {
    private static Scanner sc;
    private static int h;
    private static int m;
    private static int n;
    private static int[][] labyrinth;
    private static int[] finish;


    public static void main(String[] args) throws FileNotFoundException {
        sc = new Scanner(new File("input.txt"));
        h = sc.nextInt();
        m = sc.nextInt();
        n = sc.nextInt();
        labyrinth = getData(m * h + h - 1, n);
        finish = getPosition(labyrinth, -2);
        labyrinth[finish[0]][finish[1]] = 0;
        wave();
        PrintWriter pw = new PrintWriter(new File("output.txt"));
        pw.print((labyrinth[finish[0]][finish[1]] - 1) * 5);
        pw.close();
    }

    private static void wave() {
        Set<int[]> list;
        Set<int[]> nextList = new HashSet<>();
        int posM;
        int posN;
        int mark = 1;
        int[] point = getPosition(labyrinth, 1);
        nextList.add(point);
        while (labyrinth[finish[0]][finish[1]] == 0) {
            list = new HashSet<>(nextList);
            nextList.clear();
            for (int[] ints : list) {
                posM = ints[0];
                posN = ints[1];
                if ((posM + m + 1) < m * h + h - 1 && labyrinth[posM + m + 1][posN] == 0) {
                    labyrinth[posM + m + 1][posN] = mark + 1;
                    point = new int[]{posM + m + 1, posN};
                    nextList.add(point);
                }
//                if ((posM - m - 1) >= 0 && labyrinth[posM - m - 1][posN] == 0) {
//                    labyrinth[posM - m - 1][posN] = mark + 1;
//                }
                if ((posN + 1) < n && labyrinth[posM][posN + 1] == 0) {
                    labyrinth[posM][posN + 1] = mark + 1;
                    point = new int[]{posM, posN + 1};
                    nextList.add(point);
                }
                if ((posM + 1) < m * h + h - 1 && labyrinth[posM + 1][posN] == 0) {
                    labyrinth[posM + 1][posN] = mark + 1;
                    point = new int[]{posM + 1, posN};
                    nextList.add(point);
                }
                if ((posN - 1) >= 0 && labyrinth[posM][posN - 1] == 0) {
                    labyrinth[posM][posN - 1] = mark + 1;
                    point = new int[]{posM, posN - 1};
                    nextList.add(point);
                }
                if ((posM - 1) >= 0 && labyrinth[posM - 1][posN] == 0) {
                    labyrinth[posM - 1][posN] = mark + 1;
                    point = new int[]{posM - 1, posN};
                    nextList.add(point);
                }
            }
            mark++;
        }
    }

    private static int[][] getData(int x, int y) {
        int[][] array = new int[x][y];
        String line;
        for (int i = 0; i < array.length; i++) {
            if (i != 0 && i % m == (i / m - 1)) {
                array[i] = border();
            } else if (sc.hasNext()) {
                line = sc.next();
                array[i] = divide(line);

            }
        }
        return array;
    }

    private static int[] border() {
        int[] array = new int[n];
        for (int i = 0; i < n; i++) {
            array[i] = -1;
        }
        return array;
    }

    private static int[] divide(String line) {
        int[] array = new int[line.length()];
        line = line.trim();
        int number;
        for (int i = 0; i < line.length(); i++) {
            switch (line.substring(i, i + 1)) {
                case ".":
                    number = 0;
                    break;
                case "o":
                    number = -1;
                    break;
                case "1":
                    number = 1;
                    break;
                default:
                    number = -2;
            }
            array[i] = number;
        }
        return array;
    }

    private static int[] getPosition(int[][] array, int oneOrTwo) {
        int[] point = new int[2];
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++)
                if (array[i][j] == oneOrTwo) {
                    point[0] = i;
                    point[1] = j;
                }
        }
        return point;
    }
}