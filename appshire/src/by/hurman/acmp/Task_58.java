package by.hurman.acmp;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Scanner;

public class Task_58 {
    public static void main(String[] args) throws FileNotFoundException {
        Scanner sc = new Scanner(new File("input.txt"));
        int tables = sc.nextInt();
        int lines;
        int columns;
        int[][] array;
        String[] result = new String[tables];
        for (int i = 0; i < tables; i++) {
            lines = sc.nextInt();
            columns = sc.nextInt();
            array = new int[lines][columns];
            for (int j = 0; j < lines; j++) {
                for (int k = 0; k < columns; k++) {
                    array[j][k] = sc.nextInt();
                }
            }
            result[i] = isPretty(array);
        }
        output(result);
    }

    private static String isPretty(int[][] array) {
        String s = "YES";
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                if (array.length - i >= 2 && array[i].length - j >= 2) {
                    if (array[i][j] == 0 && array[i + 1][j + 1] == 0 && array[i + 1][j] == 0 && array[i][j + 1] == 0
                            || array[i][j] == 1 && array[i + 1][j + 1] == 1 && array[i + 1][j] == 1 && array[i][j + 1] == 1)
                        s = "NO";
                }
            }
        }
        return s;
    }

    private static void output(String[] array) throws FileNotFoundException {
        StringBuilder sb = new StringBuilder();
        PrintWriter pw = new PrintWriter(new File("output.txt"));
        for (String i : array) {
            sb.append(i).append('\n');
        }
        pw.print(sb.toString().trim());
        pw.close();
    }
}
