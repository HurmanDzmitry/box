package by.hurman.acmp;

import java.io.File;
import java.io.PrintWriter;
import java.util.Scanner;

public class Task_254 {
    public static void main(String[] args) throws Exception {
        Scanner sc = new Scanner(new File("input.txt"));
        int n = sc.nextInt();
        int[] arrayBefore = new int[n];
        int[] arrayAfter = new int[n];
        for (int i = 0; i < n; i++) {
            int x = sc.nextInt();
            arrayBefore[i] = x;
        }
        int m = sc.nextInt();
        int[] couple = new int[2];
        for (int i = 0; i < m; i++) {
            couple[0] = sc.nextInt();
            couple[1] = sc.nextInt();
            for (int j = 0; j < n; j++) {
                if (arrayBefore[j] == couple[0]) {
                    arrayAfter[j] = couple[1];
                }
            }
        }
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < n; i++) {
            if (arrayAfter[i] == 0) {
                arrayAfter[i] = arrayBefore[i];
            }
            sb.append(arrayAfter[i]).append(" ");
        }
        PrintWriter pw = new PrintWriter(new File("output.txt"));
        pw.print(sb.toString().trim());
        pw.close();
    }
}