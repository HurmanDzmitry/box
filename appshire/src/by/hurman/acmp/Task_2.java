package by.hurman.acmp;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Scanner;

public class Task_2 {
    public static void main(String[] args) throws FileNotFoundException {
        Scanner sc = new Scanner(new File("input.txt"));
        int x = sc.nextInt();
        PrintWriter pw = new PrintWriter(new File("output.txt"));
        pw.print(sum(x));
        pw.close();
    }

    private static int sum(int x) {
        if (x > 0)
            return (x + 1) * x / 2;
        else
            return (-x + 1) * x / 2 + 1;
    }
}
