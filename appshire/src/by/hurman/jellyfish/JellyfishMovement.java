package by.hurman.jellyfish;

import java.awt.*;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

//  Класс описывающий изменение состояния медуз (передвижения)
public class JellyfishMovement {

    private final char symbolTurnLeft = 'L';
    private final char symbolTurnRight = 'R';
    private final char symbolMoveForward = 'F';

    private final List<JellyfishLocation> jellyfishLocations;
    private final Point corner;
    private final Set<Point> lockedForwardPositions;

    public JellyfishMovement(List<JellyfishLocation> jellyfishLocations, Point corner) {
        this.jellyfishLocations = jellyfishLocations;
        this.corner = corner;
        lockedForwardPositions = new HashSet<>();
    }

    public void move() {
        for (JellyfishLocation jellyfishLocation : jellyfishLocations) {
            char[] actions = jellyfishLocation.getActions().toCharArray();
            for (char symbol : actions) {
                if (jellyfishLocation.isInside()) {
                    switch (symbol) {
                        case symbolTurnLeft:
                            turnLeft(jellyfishLocation);
                            break;
                        case symbolTurnRight:
                            turnRight(jellyfishLocation);
                            break;
                        case symbolMoveForward:
                            moveForward(jellyfishLocation);
                            break;
                    }
                }
            }
        }
    }

    //движение только вперед
    private void moveForward(JellyfishLocation jellyfishLocation) {
        Point position = new Point(jellyfishLocation.getPosition());
        switch (jellyfishLocation.getDirection()) {
            case NORTH:
                position.y++;
                break;
            case EAST:
                position.x++;
                break;
            case SOUTH:
                position.y--;
                break;
            case WEST:
                position.x--;
                break;
        }
        //если есть запах медузы, то движения не происходит
        if (lockedForwardPositions.contains(position)) {
            return;
        }
        //если медуза уплывает за границы
        if (position.x > corner.x || position.y > corner.y || position.x < 0 || position.y < 0) {
            lockedForwardPositions.add(position);
            jellyfishLocation.setInside(false);
            return;
        }
        //если медуза движется внутри ограниченной площади
        jellyfishLocation.setPosition(position);
    }

    //поворот направо
    private void turnRight(JellyfishLocation jellyfishLocation) {
        int nextOrdinal = (jellyfishLocation.getDirection().ordinal() + 1) % CardinalDirection.values().length;
        CardinalDirection cd = CardinalDirection.values()[nextOrdinal];
        jellyfishLocation.setDirection(cd);
    }

    //поворот налево
    private void turnLeft(JellyfishLocation jellyfishLocation) {
        int nextOrdinal = jellyfishLocation.getDirection().ordinal() - 1;
        if (nextOrdinal == -1) {
            nextOrdinal = CardinalDirection.values().length - 1;
        }
        CardinalDirection cd = CardinalDirection.values()[nextOrdinal];
        jellyfishLocation.setDirection(cd);
    }
}