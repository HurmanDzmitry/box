package by.hurman.jellyfish;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public class Main {

    //  Так как конкретики по способу чтения дынных нету, значит будем вносить
    //вручную в конструктор (чтобы наверняка), а выводить результат на консоль.
    //В условии не было уточнений по взаимодействию медуз друг с другом, поэтому
    //будем считать, что в одной точке может находиться 2 и более медузы.
    //Будем считать, что все вводимые данные корректны.

    //верхний правый угол
    private static final Point corner = new Point(5, 3);

    //информация о медузе и ее передвижении
    private static final List<JellyfishLocation> jellyfishLocations = new ArrayList<>();

    static {
        jellyfishLocations.add(new JellyfishLocation(
                new Point(1, 1), /* начальная позиция */
                CardinalDirection.EAST, /* направление */
                "RFRFRFRF")); /* инструкция движения */
        jellyfishLocations.add(new JellyfishLocation(
                new Point(3, 2), CardinalDirection.NORTH, "FRRFLLFFRRFLL"));
        jellyfishLocations.add(new JellyfishLocation(
                new Point(0, 3), CardinalDirection.WEST, "LLFFFLFLFL"));
    }

    public static void main(String[] args) {
        new JellyfishMovement(jellyfishLocations, corner).move();

        //результат на консоль
        printResult();
    }

    private static void printResult() {
        for (JellyfishLocation jellyfishLocation : jellyfishLocations) {
            System.out.print("" + jellyfishLocation.getPosition().x
                    + jellyfishLocation.getPosition().y
                    + jellyfishLocation.getDirection().name().charAt(0));
            if (!jellyfishLocation.isInside()) {
                System.out.print("LOST");
            }
            System.out.println();
        }
    }
}
