package by.hurman.jellyfish;

import java.awt.*;
import java.util.Objects;

//  Класс описывающий состояние медуз (их позиции, напрвления и движения)
public class JellyfishLocation {

    private Point position;
    private CardinalDirection direction;
    private String actions;
    private boolean inside;

    public JellyfishLocation(Point position, CardinalDirection direction, String actions) {
        this.position = position;
        this.direction = direction;
        this.actions = actions;
        inside = true;
    }

    public Point getPosition() {
        return position;
    }

    public void setPosition(Point position) {
        this.position = position;
    }

    public CardinalDirection getDirection() {
        return direction;
    }

    public void setDirection(CardinalDirection direction) {
        this.direction = direction;
    }

    public String getActions() {
        return actions;
    }

    public void setActions(String actions) {
        this.actions = actions;
    }

    public boolean isInside() {
        return inside;
    }

    public void setInside(boolean inside) {
        this.inside = inside;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        JellyfishLocation jellyfishLocation = (JellyfishLocation) o;
        return inside == jellyfishLocation.inside &&
                Objects.equals(position, jellyfishLocation.position) &&
                direction == jellyfishLocation.direction &&
                Objects.equals(actions, jellyfishLocation.actions);
    }

    @Override
    public int hashCode() {
        return Objects.hash(position, direction, actions, inside);
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + "{" +
                "position=" + position +
                ", direction=" + direction +
                ", actions='" + actions + '\'' +
                ", inside=" + inside +
                '}';
    }
}

