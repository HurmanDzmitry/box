package by.hurman;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        int[] x = {3, 2, 1, 0};
//        Arrays.sort(x);

        for (int i = 0; i < x.length; i++) {
            for (int j = i + 1; j < x.length; j++) {
                int a;
                if (x[i] > x[j]) {
                    a = x[i];
                    x[i] = x[j];
                    x[j] = a;
                }
            }
        }

        System.out.println(Arrays.toString(x));
    }
}
