Клонируйте данный репозиторий.
В папке resliv находится файл run.bat, запустите его и подождите окончания загрузки
(закрыть после окончания работы).

Откройте Telegram, найдите бот name - InfoCityBot, surname - InfoCityHurmanBot.
(эти данные, как и токен, можно найти в файле bot.properties в папке resources проекта)
Вводите названия городов (их в базе пока 5: Минск, Москва, Киев, Варшава, Вильнюс).


Также можно открыть браузер и ввести в строку запроса http://localhost:8080/.

Для управления данными воспользоваться новым окном командной строки (или в браузере при get запросах).
Примеры команд:
//show all city (get)
curl localhost:8080/city
//show city with choose id (get)
curl localhost:8080/city/1
//create or update city
curl localhost:8080/city/create -d id=6 -d name="Рига" -d info="ББББ ВВВВ СССС"
curl localhost:8080/city/update -d id=2 -d name="АААА" -d info="ББББ ВВВВ СССС"
//drop city
curl --request DELETE localhost:8080/city/delete/6

//show info about city with choose id (get)
curl localhost:8080/info/2
//update info
curl localhost:8080/info/update -d id=2 -d info="ББББ ВВВВ ФФФФФФФФ"
//add info
curl localhost:8080/info/add -d id=2 -d info=" ЯЯЯЯЯЯЯЯЯЯЯЯ"
//drop info
curl --request POST localhost:8080/info/delete/2

//for cmd
chcp 65001
