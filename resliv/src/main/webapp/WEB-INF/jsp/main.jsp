<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Info</title>
</head>
<body>
<jsp:useBean id="city" scope="request" class="by.hurman_dzmitry.entity.City"/>
Введит название города.<br>
<form method="get" action="${pageContext.request.contextPath}/showInfo">
    <label>Город:
        <input type="text" name="name"><br/>
    </label>
    <button type="submit">Submit</button>
</form>
<c:if test="${city.id != null}">
    <h3>${city.name}:</h3>
    ${city.info} <br/>
</c:if>
<jsp:useBean id="message" scope="request" class="java.lang.String"/>
<c:if test="${message == 'error'}">
    Информации о таком городе нет! <br/>
</c:if>
</body>
</html>
