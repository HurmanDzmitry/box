package by.hurman_dzmitry.service;

import by.hurman_dzmitry.entity.City;
import by.hurman_dzmitry.exception.RecordNotFoundException;
import by.hurman_dzmitry.repository.CityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class InfoCityService {

    @Autowired
    CityRepository repository;

    @Autowired
    CityService cityService;

    public String getInfoByCityId(Long id) throws RecordNotFoundException {
        return cityService.getCityById(id).getInfo();
    }

    public City updateInfoAboutCityByCityId(Long id, String info) throws RecordNotFoundException {
        Optional<City> cityOptional = repository.findById(id);

        if (cityOptional.isPresent()) {
            City city = cityOptional.get();
            city.setInfo(info);
            city = repository.save(city);
            return city;
        } else {
            throw new RecordNotFoundException("Data not found for given city id");
        }
    }

    public City addInfoAboutCityByCityId(Long id, String info) throws RecordNotFoundException {
        Optional<City> cityOptional = repository.findById(id);

        if (cityOptional.isPresent()) {
            City city = cityOptional.get();
            city.setInfo(city.getInfo() + info);
            city = repository.save(city);
            return city;
        } else {
            throw new RecordNotFoundException("Data not found for given city id");
        }
    }

    public void deleteInfoAboutCityByCityId(Long id) throws RecordNotFoundException {
        Optional<City> cityOptional = repository.findById(id);

        if (cityOptional.isPresent()) {
            City city = cityOptional.get();
            city.setInfo("");
            repository.save(city);
        } else {
            throw new RecordNotFoundException("Data not found for given city id");
        }
    }
}
