package by.hurman_dzmitry.service;

import by.hurman_dzmitry.entity.City;
import by.hurman_dzmitry.exception.RecordNotFoundException;
import by.hurman_dzmitry.repository.CityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CityService {

    @Autowired
    CityRepository repository;

    public List<City> getAllCities() throws RecordNotFoundException {
        List<City> cityList = repository.findAll();

        if (cityList.size() > 0) {
            return cityList;
        } else {
            throw new RecordNotFoundException("Data not found");
        }
    }

    public City getCityById(Long id) throws RecordNotFoundException {
        Optional<City> cityOptional = repository.findById(id);

        if (cityOptional.isPresent()) {
            return cityOptional.get();
        } else {
            throw new RecordNotFoundException("Data not found for given city id");
        }
    }

    public City getCityByName(String name) throws RecordNotFoundException {
        Optional<City> cityOptional = repository.findByName(name);

        if (cityOptional.isPresent()) {
            return cityOptional.get();
        } else {
            throw new RecordNotFoundException("Data not found for given city name");
        }
    }

    public City createOrUpdateCity(City entity) {
        Optional<City> cityOptional = repository.findById(entity.getId());

        if (cityOptional.isPresent()) {
            City city = cityOptional.get();
            city.setName(entity.getName());
            city.setInfo(entity.getInfo());
            city = repository.save(city);
            return city;
        } else {
            entity = repository.save(entity);
            return entity;
        }
    }

    public void deleteCityById(Long id) throws RecordNotFoundException {
        Optional<City> cityOptional = repository.findById(id);

        if (cityOptional.isPresent()) {
            repository.deleteById(id);
        } else {
            throw new RecordNotFoundException("Data not found for given city id");
        }
    }
}