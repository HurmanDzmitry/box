package by.hurman_dzmitry.controller;

import by.hurman_dzmitry.entity.City;
import by.hurman_dzmitry.exception.RecordNotFoundException;
import by.hurman_dzmitry.service.InfoCityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/info")
public class CityInfoRestController {

    @Autowired
    InfoCityService service;

    @GetMapping("/{id}")
    public ResponseEntity<String> getInfo(@PathVariable("id") Long id) throws RecordNotFoundException {
        String entity = service.getInfoByCityId(id);
        return new ResponseEntity<>(entity, HttpStatus.OK);
    }

    @PostMapping("/update")
    public ResponseEntity<City> updateInfo(@RequestParam  Long id, @RequestParam String info)
            throws RecordNotFoundException {
        City entity = service.updateInfoAboutCityByCityId(id, info);
        return new ResponseEntity<>(entity, HttpStatus.OK);
    }

    @PostMapping("/add")
    public ResponseEntity<City> addInfo(@RequestParam  Long id, @RequestParam String info)
            throws RecordNotFoundException {
        City updated = service.addInfoAboutCityByCityId(id, info);
        return new ResponseEntity<>(updated, HttpStatus.OK);
    }

    @PostMapping("/delete/{id}")
    public HttpStatus deleteInfo(@PathVariable("id")  Long id) throws RecordNotFoundException {
        service.deleteInfoAboutCityByCityId(id);
        return HttpStatus.OK;
    }
}
