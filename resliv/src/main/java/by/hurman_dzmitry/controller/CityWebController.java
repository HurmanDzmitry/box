package by.hurman_dzmitry.controller;

import by.hurman_dzmitry.entity.City;
import by.hurman_dzmitry.exception.RecordNotFoundException;
import by.hurman_dzmitry.service.CityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("/")
public class CityWebController {

    @Autowired
    CityService service;

    @GetMapping("/showInfo")
    public String cityPage(@RequestParam String name, Model model) {
        City city = null;
        try {
            city = service.getCityByName(name);
        } catch (RecordNotFoundException e) {
            model.addAttribute("message", "error");
        }
        model.addAttribute("city", city);
        return "main";
    }

    @GetMapping("/")
    public String welcome() {
        return "main";
    }
}
