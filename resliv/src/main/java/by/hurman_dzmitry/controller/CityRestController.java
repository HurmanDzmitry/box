package by.hurman_dzmitry.controller;

import by.hurman_dzmitry.entity.City;
import by.hurman_dzmitry.exception.RecordNotFoundException;
import by.hurman_dzmitry.service.CityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/city")
public class CityRestController {

    @Autowired
    CityService service;

    @GetMapping
    public ResponseEntity<List<City>> getAllCities() throws RecordNotFoundException {
        List<City> list = service.getAllCities();
        return new ResponseEntity<>(list, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<City> getCityById(@PathVariable("id") Long id) throws RecordNotFoundException {
        City entity = service.getCityById(id);
        return new ResponseEntity<>(entity, HttpStatus.OK);
    }

    @PostMapping({"/create", "/update"})
    public ResponseEntity<City> createOrUpdateCity(@RequestParam Long id, @RequestParam String name, @RequestParam String info) {
        City city = new City(name, info);
        city.setId(id);
        City updated = service.createOrUpdateCity(city);
        return new ResponseEntity<>(updated, HttpStatus.OK);
    }

    @DeleteMapping("/delete/{id}")
    public HttpStatus deleteCityById(@PathVariable("id") Long id) throws RecordNotFoundException {
        service.deleteCityById(id);
        return HttpStatus.OK;
    }
}
