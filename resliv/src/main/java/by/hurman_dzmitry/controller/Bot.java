package by.hurman_dzmitry.controller;

import by.hurman_dzmitry.exception.RecordNotFoundException;
import by.hurman_dzmitry.service.CityService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

@Component
@PropertySource("classpath:bot.properties")
public class Bot extends TelegramLongPollingBot {

    private static final Logger logger = LoggerFactory.getLogger(Bot.class);

    @Autowired
    CityService service;

    @Value("${bot.token}")
    private String token;

    @Value("${bot.username}")
    private String username;

    @Override
    public String getBotToken() {
        return token;
    }

    @Override
    public String getBotUsername() {
        return username;
    }

    @Override
    public void onUpdateReceived(Update update) {
        if (update.hasMessage()) {
            Message message = update.getMessage();
            SendMessage response = new SendMessage();
            Long chatId = message.getChatId();
            response.setChatId(chatId);
            String text;
            try {
                String request = message.getText();
                if (request.equals("/start")) {
                    text = "Вводите название города.";
                } else {
                    text = service.getCityByName(request).getInfo();
                }
            } catch (RecordNotFoundException e) {
                text = "Информации о таком городе нет!";
            }
            response.setText(text);
            try {
                execute(response);
            } catch (TelegramApiException e) {
                logger.error("Failed to send message", text, chatId, e.getMessage());
            }
        }
    }
}
