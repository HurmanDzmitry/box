package by.hurman_dzmitry.repository;

import by.hurman_dzmitry.entity.City;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.Optional;

public interface CityRepository extends JpaRepository<City, Long>,
        CrudRepository<City, Long>,
        PagingAndSortingRepository<City, Long> {

    Optional<City> findByName(String name);
}
