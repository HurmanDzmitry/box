package ch02;

public interface MessageRenderer {

    void render();

    void setMessageProvider(MessageProvider provider);
}
