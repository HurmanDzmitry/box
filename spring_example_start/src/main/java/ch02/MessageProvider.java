package ch02;

public interface MessageProvider {

    String getMessage();
}
